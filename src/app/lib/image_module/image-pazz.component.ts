import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Import Shared Service
import { SubjectsService } from '../../service/subjects.service';
// Import Animation

// Import Lib Service
import { ImageSaveService } from './service/image-save.service';

@Component({
    selector: 'image-pazz',
    templateUrl: './image-pazz.component.html',
    styleUrls: ['./loader.scss']
})

export class ImagePazzComponent {

    reader = [];        // ローカルファイルデータ
    orgFiles: FileList;      // 読み込みデータ
    imageFiles = [];    // 画像データ（名前、ファイル形式、base64データ）
    fileCount = 0;      // 読み込みファイル数の合計
    loadedCount = 0;    // 読み込んだファイル数
    baseImage;
    resultImage;

    showFlags = [];
    showUpBase = true;
    showBox = false;
    showReload = false;
    showResultImage = false;
    showBaseImage = true;

    onLoad = false;
    onUp = false;
    onDrag = false;
    onImage = '';



    subscription: Subscription;
    constructor(
        private router: Router,
        private subjectsService: SubjectsService,
        private imagesaveService: ImageSaveService
    ) {
        this.reader[0] = new FileReader();
    }

    /****************************************************
     *
     * ドラッグイベント
     *
     ***************************************************/


    /**
     *
     * @param event
     */
    onDragOverHandler(event: DragEvent): void {
        this.onDrag = true;
        event.preventDefault();

    }
    onDragLeaveHandler(event: DragEvent): void {
        this.onDrag = false;
        event.stopPropagation();
    }

    /**
     * ファイルドロップイベント
     * @param event ドラッグされたファイル
     */
    onDropHandler(event: DragEvent): void {
        event.preventDefault();
        this.onDrag = false;
        this.showBox = true;
        // 読み込み中表示
        this.onLoad = true;
        // 各変数初期化
        this.reset('fast');
        this.orgFiles = event.dataTransfer.files;
        this.fileCount = this.orgFiles.length;

        // データタイプの判定 読み込み
        if (!this.orgFiles[0] || this.orgFiles[0].type.indexOf('image/') < 0) {
        } else {

            // ファイルが1つの場合
            if (this.fileCount <= 1) {
                this.reader[0] = new FileReader();
                this.reader[0].onloadend = (e) => {
                    // 読み込み中表示消す
                    const imageType = this.getFileType(e.target.result);
                    const imageName = this.getRandomName();
                    this.onLoad = false;
                    this.imageFiles.push({
                        name: this.orgFiles[0].name,
                        type: imageType,
                        data: e.target.result});
                    this.switchWindow(e);
                };
                this.reader[0].readAsDataURL(this.orgFiles[0]);

            // ファイルが複数の場合
            } else if (this.fileCount > 1) {
                const readimageData = [];
                for (const key in this.orgFiles) {
                    if (this.orgFiles.hasOwnProperty(key)) {
                        this.reader[key] = new FileReader();
                        this.reader[key].onloadend = (e) => {
                            const imageType = this.getFileType(e.target.result);
                            const imageName = this.getRandomName();
                            try {
                                readimageData[key] = {
                                    name: this.orgFiles[key].name,
                                    type: imageType,
                                    data: e.target.result};

                                    this.loadedCount++;
                                if (this.loadedCount === this.fileCount) {
                                    // 読み込み中表示消す
                                    this.onLoad = false;
                                    this.imageFiles = readimageData;
                                    this.catImage();
                                    this.switchWindow(e);
                                }
                            } catch (error) {
                                console.log('image read error' + error);
                            }
                        };
                        this.reader[key].readAsDataURL(this.orgFiles[key]);
                    }
                }
            }
        }
        event.stopPropagation();
    }

    /**
     * グリッド画像のドロップ
     * @param event ドラッグされたファイル
     */
    onDropBaseHandler(event: DragEvent): void {
        event.preventDefault();

        // 読み込み中表示
        this.onLoad = true;
        const readFiles = event.dataTransfer.files;

        // データタイプの判定 読み込み
        if (!readFiles[0] || readFiles[0].type.indexOf('image/') < 0) {
        } else {

            const reader = new FileReader();
            reader.onloadend = (e: any) => {
                // 読み込み中表示消す
                this.onLoad = false;
                this.showBaseImage = false;
                this.baseImage = e.target.result;
                this.gridBuild();
            };
            reader.readAsDataURL(readFiles[0]);

        }
        event.stopPropagation();
    }

    /**
     * 画像のトリミング
     * リサイズ処理を行っているので
     * 切り取り位置の指定は縮小率を考慮する必要あり
     */
    catImage(): void {
        for (const key in this.imageFiles) {
            if (this.imageFiles.hasOwnProperty(key)) {
                const oc = <HTMLCanvasElement> document.createElement('canvas');
                const octx = oc.getContext('2d');
                const img = new Image();
                img.onload = (e) => {
                    oc.setAttribute('width', (100).toString());
                    oc.setAttribute('height', (100).toString());
                    octx.drawImage(
                        img, // 切り取りイメージ
                        21.4, // 切り取り開始X座標
                        610.7, // 切り取り開始Y座標
                        105.1, // 切り取り幅
                        105.1, // 切り取り高さ
                        0, 0,
                        100,
                        100); // 切り取り後の表示位置とサイズ

                        // 切り取り後のイメージを保存
                    this.imageFiles[key]['data'] = oc.toDataURL(this.imageFiles[key]['type']);
                };
                img.src = this.imageFiles[key]['data'];
            }
        }
    }

    /**
     * グリッド表示イメージ作成
     */
    gridBuild(): void {
        const oc = <HTMLCanvasElement> document.createElement('canvas');
        const octx = oc.getContext('2d');
        const base = new Image();
        base.onload = (e) => {
            oc.setAttribute('width', (1920).toString());
            oc.setAttribute('height', (1920).toString());
            octx.drawImage(base, 0, 0, 1920, 1920);

            let count = 0;
            for (const key in this.imageFiles) {
                if (this.imageFiles.hasOwnProperty(key)) {
                    const img = new Image();
                    const position = this.mathPosition(this.imageFiles[key].name);
                    console.log(position['x'] + '::' + position['y']);
                    img.onload = (event) => {
                        octx.drawImage(img, position['x'], position['y'], 96, 96);
                        if (this.loadedCount === count) {
                            this.resultImage = oc.toDataURL('image/jpeg');
                            this.showResultImage = true;
                        }
                    };
                    img.src = this.imageFiles[key]['data']
                    count++;
                }
            }
        };
        base.src = this.baseImage;
    }
    /**
     *
     * @param file ファイル名
     */
    mathPosition(file: string) {
        console.log(file);
        const name = file.split('.');
        const num = Number(name[0]);
        if (num < 0) {
            return false;
        }

        const page = Math.floor(num / 400);
        const shelf = num - page * 400;
        const shelf_line = Math.floor(shelf / 20);
        const shelf_room = shelf - shelf_line * 20 - 1;
        const positionX = shelf_room * 96;
        const positionY = shelf_line * 96;

        return {x: positionX, y: positionY};
    }
    /********************************************
     *
     * 後始末、初期化、その他
     *
     *******************************************/
    /**
     * 表示の切り替え
     * @param event マウスイベント
     */
    switchWindow(event): void {
        this.onUp = true;
        this.showUpBase = false;
        this.showReload = true;
        this.showBaseImage = true;
        this.showResultImage = false;
    }

    /**
     * マウスイベント削除
     * angularに依存せずにイベントを管理しているので必ず
     * イベントを毎回破棄すること
     */

    reset(type: string): void {
        if (type === 'last') {

        } else if (type === 'first') {
            this.reader = [];
            this.imageFiles = [];
            this.loadedCount = 0;
        }
    }
    getFileType(file): string {
        const header = file.split(';');
        const type = header[0].split(':');
        return type[1];
    }
    getRandomName(num: number = 16): string {
        return Math.random().toString(36).slice(-(num));
    }

    saveImage(): void {
        this.imagesaveService.setParam(
            'base',
            'image/jpg',
            this.resultImage
        );

        this.imagesaveService.saveImage();
    }

    close(): void {
        this.subjectsService.publish('closer');
    }
}
