import { Component } from '@angular/core';
import { LocalDataService } from '../../service/local-data.service';

@Component({
    selector: 'lib-callender',
    templateUrl: 'callender.html',
    styleUrls: ['callender.css']
})

export class CallenderComponent {

    setArousals: Array<number> = new Array(0);

    constructor(
        private localDataService: LocalDataService
    ) {
    }

}
