"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
// import Local Share Service
var subjects_service_1 = require("../../service/subjects.service");
// Import Animation
var ClockSelectComponent = (function () {
    function ClockSelectComponent(router, location, subjectsService) {
        this.router = router;
        this.location = location;
        this.subjectsService = subjectsService;
        // clock option
        this._cx = 150;
        this._cy = 150;
        this._strokeWidth = 1;
        this._showHour = true;
        this._showMinits = false;
        // Hour Clock View Setting
        this._amR = 100;
        this._amR_BarSize = 60;
        this._pmR = 60;
        this._pmR_BarSize = 20;
        this._textCy = 155;
        this._amTextR = 85;
        this._pmTextR = 45;
        this._baseColor = 'rgba(0,0,0,0)';
        this._enterColor = 'rgba(127,255,212,0.8)';
        // Hour Clock View Setting
        this._minitsR = 100;
        this._minitsR_BarSize = 40;
        this._minitsTextR = 85;
        this.setHour = 0;
        this.setMinits = 0;
        this.setSecond = 0;
        this.setAMPM = 0;
        // Hour Data
        this.onCtrl = false;
        // マウスイベント
        this.onMousemove = false;
        this.onMousedown = false;
        this.enterCellColor = [];
        this.enterMinCellColor = [];
        this.checkTouch = '';
        // Build Hour Time
        this.amTimesPosition = this.buildTime(this._amTextR, 12, 12);
        this.pmTimesPosition = this.buildTime(this._pmTextR, 24, 12);
        this.amTimesPath = this.buildPath(this._cx, this._cy, this._amR, this._amR_BarSize, 12);
        this.pmTimesPath = this.buildPath(this._cx, this._cy, this._pmR, this._pmR_BarSize, 12);
        //  Build Minits Time
        this.minitsPosition = this.buildMinits(this._minitsTextR, 10);
        this.minitsPath = this.buildPath(this._cx, this._cy, this._minitsR, this._minitsR_BarSize, 6);
        if (window.ontouchstart === null) {
            this.checkTouch = 'mu~ri~';
        }
        else {
            this.checkTouch = 'ばっちこい';
        }
        console.log(this.amTimesPosition);
    }
    ClockSelectComponent.prototype.setMoveDisabled = function () {
        window.addEventListener('touchmove', function (event) {
            event.preventDefault();
        }, true);
    };
    ClockSelectComponent.prototype.setMoveEnabled = function () {
        window.addEventListener('touchmove', function (event) {
            event.stopPropagation();
        }, true);
    };
    ClockSelectComponent.prototype.setTime = function (time) {
        this.subjectsService.publish('update-time', this.selectTime);
    };
    ClockSelectComponent.prototype.buildTime = function (r, time, step) {
        var baseTime = time - step;
        var positions = [];
        for (var i = baseTime; i < time; i++) {
            var angle = i * (360 / step) - 90;
            var radian = angle * Math.PI / 180;
            var xx = Math.floor(r * Math.cos(radian));
            var yy = Math.floor(r * Math.sin(radian));
            var val = void 0;
            if (i === 0 || i === 12) {
                val = [time, xx, yy];
                this.enterCellColor[time] = this._baseColor;
            }
            else {
                val = [i, xx, yy];
                this.enterCellColor[i] = this._baseColor;
            }
            positions.push(val);
        }
        return positions;
    };
    ClockSelectComponent.prototype.buildMinits = function (r, step) {
        var positions = [];
        var count = 60 / step;
        for (var i = 0; i < count; i++) {
            var angle = i * (360 / count) - 90;
            var radian = angle * Math.PI / 180;
            var xx = Math.floor(r * Math.cos(radian));
            var yy = Math.floor(r * Math.sin(radian));
            var val = void 0;
            val = [(step * i), xx, yy];
            this.enterMinCellColor[(step * i)] = this._baseColor;
            positions.push(val);
        }
        return positions;
    };
    ClockSelectComponent.prototype.buildPath = function (cx, cy, rOut, rIn, steps) {
        if (steps === void 0) { steps = 12; }
        var paths = [];
        var startDegree = 360 - 360 / steps / 2;
        var finishDegree = startDegree + 360 / steps;
        for (var i = 0; i < steps; i++) {
            var startX = cx + rIn * Math.sin(startDegree / 180 * Math.PI);
            var startY = cy - rIn * Math.cos(startDegree / 180 * Math.PI);
            var OutX = cx + rOut * Math.sin(startDegree / 180 * Math.PI);
            var OutY = cy - rOut * Math.cos(startDegree / 180 * Math.PI);
            var InX = cx + rIn * Math.sin(finishDegree / 180 * Math.PI);
            var InY = cy - rIn * Math.cos(finishDegree / 180 * Math.PI);
            var ArkXOut = cx + rOut * Math.sin(finishDegree / 180 * Math.PI);
            var ArkYOut = cy - rOut * Math.cos(finishDegree / 180 * Math.PI);
            var ArkXIn = cx + rIn * Math.sin(startDegree / 180 * Math.PI);
            var ArkYIn = cy - rIn * Math.cos(startDegree / 180 * Math.PI);
            var largeArcFlag = (finishDegree - startDegree <= 180) ? 0 : 1;
            var move = 'M ' + startX + ' ' + startY + ' ';
            var lineOut = 'L ' + OutX + ' ' + OutY + ' ';
            var arcOut = 'A ' + rOut + ' ' + rOut + ' ' + 0 + ' ' + largeArcFlag + ' ' + 1 + ' ' + ArkXOut + ' ' + ArkYOut + ' ';
            var lineIn = 'L ' + InX + ' ' + InY + ' ';
            var arcIn = 'A ' + rIn + ' ' + rIn + ' ' + 0 + ' ' + largeArcFlag + ' ' + 0 + ' ' + ArkXIn + ' ' + ArkYIn + ' ';
            var close_1 = 'Z';
            paths[i] = move + lineOut + arcOut + lineIn + arcIn + close_1;
            startDegree = finishDegree;
            finishDegree = startDegree + 360 / steps;
        }
        return paths;
    };
    ClockSelectComponent.prototype.onMouseenterHandler = function (num, target) {
        this.resetColor(target);
        this.setMoveDisabled();
        if (target === 'hour' && this.onMousedown) {
            this.addHour(num);
            this.enterCellColor[num] = this._enterColor;
        }
        else if (target === 'min' && this.onMousedown) {
            this.addMinits(num);
            this.enterMinCellColor[num] = this._enterColor;
        }
    };
    ClockSelectComponent.prototype.onMouseleaveHandler = function (num, target) {
        this.resetColor(target);
    };
    ClockSelectComponent.prototype.onMousedownHandler = function (num, target) {
        this.onMousedown = true;
        if (target === 'hour') {
            this.addHour(num);
            this.enterCellColor[num] = this._enterColor;
        }
        else if (target === 'min') {
            this.addMinits(num);
            this.enterMinCellColor[num] = this._enterColor;
        }
    };
    ClockSelectComponent.prototype.onMouseupHandler = function (num, target) {
        this.setMoveEnabled();
        this.onMousedown = false;
        this.resetColor('hour');
        this.resetColor('min');
        if (this._showHour) {
            this._showHour = false;
            this._showMinits = true;
        }
        if (target === 'min') {
            // this operation is goback to return page
            // this.location.back();
            // this operation is send back to shared service
            var clockData = { hour: this.setHour, min: this.setMinits };
            this.subjectsService.publish('clock-data', clockData);
        }
    };
    ClockSelectComponent.prototype.onSwitch = function () {
        if (this._showMinits) {
            this._showHour = true;
            this._showMinits = false;
        }
        else {
            this._showHour = false;
            this._showMinits = true;
        }
    };
    ClockSelectComponent.prototype.addHour = function (num) {
        this.setHour = num;
    };
    ClockSelectComponent.prototype.addMinits = function (num) {
        this.setMinits = num;
    };
    ClockSelectComponent.prototype.resetColor = function (target) {
        if (target === 'hour') {
            for (var key in this.enterCellColor) {
                if (this.enterCellColor.hasOwnProperty(key)) {
                    this.enterCellColor[key] = this._baseColor;
                }
            }
        }
        else if (target === 'min') {
            for (var key in this.enterMinCellColor) {
                if (this.enterMinCellColor.hasOwnProperty(key)) {
                    this.enterMinCellColor[key] = this._baseColor;
                }
            }
        }
    };
    ClockSelectComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    ClockSelectComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        // this.router.navigate(['/admin']);
        return false;
    };
    ClockSelectComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return ClockSelectComponent;
}());
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], ClockSelectComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], ClockSelectComponent.prototype, "onKeyupHandler", null);
ClockSelectComponent = __decorate([
    core_1.Component({
        selector: 'clock-select',
        templateUrl: './clock-select.component.html',
        styleUrls: ['./clock.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        subjects_service_1.SubjectsService])
], ClockSelectComponent);
exports.ClockSelectComponent = ClockSelectComponent;
//# sourceMappingURL=clock-select-touch.component.js.map