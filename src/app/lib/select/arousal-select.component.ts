import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Arousal, ArousalService } from '../../service/index';
import { LocalDataService } from '../../service/local-data.service';

@Component({
    selector: 'arousal-select',
    templateUrl: './arousal-select.html',
    styleUrls: ['./select.scss']
})

export class ArousalSelectComponent {

    public arousal: Arousal = new Arousal();

    setArousals: Array<number> = new Array(0);
    arousals: Arousal[];
    selectedArousal: any;

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private arousalservice: ArousalService
    ) {
        this.arousalservice
            .getArousals()
            .then((arousals: Arousal[]) => this.arousals = arousals);
        if (this.localDataService.toChildDataArousal$.toString().length > 1) {
            this.selectedArousal = this.localDataService.toChildDataArousal$.toString();
        }
    }

    complete(): void {
        let last = this.setArousals.pop();

        for (let arousal in this.setArousals) {
            if (this.setArousals.hasOwnProperty(arousal)) {
                if (last === Number(arousal)) {
                    this.selectedArousal = this.selectedArousal + arousal;
                    this.localDataService.sendToParent(this.selectedArousal);
                } else {
                    this.selectedArousal = this.selectedArousal + arousal + ':';
                }
            }
        }
    }

    addArousal(id: number): void {
        if (this.setArousals.length >= 1) {
            this.setArousals.push(id);
        } else {
            this.setArousals = [id];
        }
    }

    delArousal(id: number): void {
        console.log(id);
        this.setArousals.splice((id), 1);
    }
}
