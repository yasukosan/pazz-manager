import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Mtype } from '../../service/mtype';
import { MtypeService } from '../../service/mtype.service';
import { LocalDataService } from '../../service/local-data.service';

@Component({
    selector: 'type-select',
    templateUrl: './type-select.html',
    styleUrls: ['./select.scss']
})

export class TypeSelectComponent {

    public mtype: Mtype = new Mtype();

    mtypes: Mtype[];

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private mtypeservice: MtypeService
    ) {
        this.mtypeservice
            .getMtypes()
            .then((mtypes: Mtype[]) => this.mtypes = mtypes);
    }

    selectType(mtype: number): void {
        this.localDataService.sendToParent(this.mtypes[mtype]);
    }
}
