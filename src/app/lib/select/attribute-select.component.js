"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var attribute_1 = require("../../service/attribute");
var attribute_service_1 = require("../../service/attribute.service");
var local_data_service_1 = require("../../service/local-data.service");
var AttributeSelectComponent = (function () {
    function AttributeSelectComponent(router, location, localDataService, attributeservice) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.attributeservice = attributeservice;
        this.attribute = new attribute_1.Attribute();
        this.attributeservice
            .getAttributes()
            .then(function (attributes) { return _this.attributes = attributes; });
    }
    AttributeSelectComponent.prototype.selectType = function (attribute) {
        this.localDataService.sendToParent(this.attributes[attribute]);
    };
    return AttributeSelectComponent;
}());
AttributeSelectComponent = __decorate([
    core_1.Component({
        selector: 'attribute-select',
        templateUrl: './attribute-select.html',
        styleUrls: ['./select.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        attribute_service_1.AttributeService])
], AttributeSelectComponent);
exports.AttributeSelectComponent = AttributeSelectComponent;
//# sourceMappingURL=attribute-select.component.js.map