"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var mtype_1 = require("../../service/mtype");
var mtype_service_1 = require("../../service/mtype.service");
var local_data_service_1 = require("../../service/local-data.service");
var TypeSelectComponent = (function () {
    function TypeSelectComponent(router, location, localDataService, mtypeservice) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.mtypeservice = mtypeservice;
        this.mtype = new mtype_1.Mtype();
        this.mtypeservice
            .getMtypes()
            .then(function (mtypes) { return _this.mtypes = mtypes; });
    }
    TypeSelectComponent.prototype.selectType = function (mtype) {
        this.localDataService.sendToParent(this.mtypes[mtype]);
    };
    return TypeSelectComponent;
}());
TypeSelectComponent = __decorate([
    core_1.Component({
        selector: 'type-select',
        templateUrl: './type-select.html',
        styleUrls: ['./select.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        mtype_service_1.MtypeService])
], TypeSelectComponent);
exports.TypeSelectComponent = TypeSelectComponent;
//# sourceMappingURL=type-select.component.js.map