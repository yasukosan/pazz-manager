import { Component, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
// import Local Share Service
import { SubjectsService } from '../../service/subjects.service';
// Import Animation

@Component({
    selector: 'clock-select',
    templateUrl: './clock-select.component.html',
    styleUrls: ['./clock.scss']
})

export class ClockSelectComponent {
    // clock option
    _cx: number = 150;
    _cy: number = 150;
    _strokeWidth: number = 1;
    _showHour: boolean = true;
    _showMinits: boolean = false;

    // Hour Clock View Setting
    _amR: number = 100;
    _amR_BarSize: number = 60;
    _pmR: number = 60;
    _pmR_BarSize: number = 20;
    _textCy: number = 155;
    _amTextR: number = 85;
    _pmTextR: number = 45;

    _baseColor: string = 'rgba(0,0,0,0)';
    _enterColor: string = 'rgba(127,255,212,0.8)';
    // Hour Clock View Setting
    _minitsR: number = 100;
    _minitsR_BarSize: number = 40;
    _minitsTextR: number = 85;



    // set Time Data
    selectTime: number;
    setHour: number = 0;
    setMinits: number = 0;
    setSecond: number = 0;
    setAMPM: number = 0;
    // Hour Data
    onCtrl: boolean = false;
    amTimesPath: string[];
    amTimesPosition: any[];
    pmTimesPath: string[];
    pmTimesPosition: any[];
    // Minits Data
    minitsPosition: any[];
    minitsPath: any[];
    // マウスイベント
    onMousemove: boolean = false;
    onMousedown: boolean = false;

    enterCellColor: string[] = [];
    enterMinCellColor: string[] = [];

    constructor(
        private router: Router,
        private location: Location,
        private subjectsService: SubjectsService,
    ) {
        // Build Hour Time
        this.amTimesPosition = this.buildTime(this._amTextR, 12, 12);
        this.pmTimesPosition = this.buildTime(this._pmTextR, 24, 12);
        this.amTimesPath = this.buildPath(
            this._cx, this._cy, this._amR, this._amR_BarSize, 12);
        this.pmTimesPath = this.buildPath(
            this._cx, this._cy, this._pmR, this._pmR_BarSize, 12);

        //  Build Minits Time
        this.minitsPosition = this.buildMinits(this._minitsTextR, 10);
        this.minitsPath = this.buildPath(
            this._cx, this._cy, this._minitsR, this._minitsR_BarSize, 6);

        window.addEventListener('touchmove', function(event) {
            event.preventDefault();
        });
        console.log(this.amTimesPosition);
    }

    setTime(time: Number): void {
        this.subjectsService.publish('update-time', this.selectTime);
    }
    buildTime(r: number, time: number, step: number): any[] {
        const baseTime = time - step;
        const positions: any[] = [];

        for (let i = baseTime; i < time; i++) {
            const angle = i * (360 / step) - 90;
            const radian = angle * Math.PI / 180;

            const xx: Number = Math.floor(r * Math.cos(radian));
            const yy: Number = Math.floor(r * Math.sin(radian));

            let val;
            if (i === 0 || i === 12) {
                val = [time, xx, yy];
                this.enterCellColor[time] = this._baseColor;
            } else {
                val = [i, xx, yy];
                this.enterCellColor[i] = this._baseColor;
            }
            positions.push(val);

        }
        return positions;
    }
    buildMinits(r: number, step: number): any[] {
        const positions: any[] = [];
        const count = 60 / step;

        for (let i = 0; i < count; i++) {
            const angle = i * (360 / count) - 90;
            const radian = angle * Math.PI / 180;

            const xx: Number = Math.floor(r * Math.cos(radian));
            const yy: Number = Math.floor(r * Math.sin(radian));

            let val;
            val = [(step * i), xx, yy];
            this.enterMinCellColor[(step * i)] = this._baseColor;
            positions.push(val);

        }
        return positions;
    }
    buildPath(cx: number, cy: number, rOut: number, rIn: number, steps: number = 12): string[] {
        const paths: string[] = [];
        let startDegree = 360 - 360 / steps / 2;
        let finishDegree = startDegree + 360 / steps;


        for (let i = 0; i < steps; i++) {
            const startX = cx + rIn * Math.sin(startDegree / 180 * Math.PI);
            const startY = cy - rIn * Math.cos(startDegree / 180 * Math.PI);
            const OutX = cx + rOut * Math.sin(startDegree / 180 * Math.PI);
            const OutY = cy - rOut * Math.cos(startDegree / 180 * Math.PI);
            const InX = cx + rIn * Math.sin(finishDegree / 180 * Math.PI);
            const InY = cy - rIn * Math.cos(finishDegree / 180 * Math.PI);

            const ArkXOut = cx + rOut * Math.sin(finishDegree / 180 * Math.PI);
            const ArkYOut = cy - rOut * Math.cos(finishDegree / 180 * Math.PI);

            const ArkXIn = cx + rIn * Math.sin(startDegree / 180 * Math.PI);
            const ArkYIn = cy - rIn * Math.cos(startDegree / 180 * Math.PI);

            const largeArcFlag = (finishDegree - startDegree <= 180) ? 0 : 1;

            const move = 'M ' + startX + ' ' + startY + ' ';
            const lineOut = 'L ' + OutX + ' ' + OutY + ' ';
            const arcOut = 'A ' + rOut + ' ' + rOut + ' ' + 0 + ' ' + largeArcFlag + ' ' + 1 + ' ' + ArkXOut + ' ' + ArkYOut + ' ';
            const lineIn = 'L ' + InX + ' ' + InY + ' ';
            const arcIn = 'A ' + rIn + ' ' + rIn + ' ' + 0 + ' ' + largeArcFlag + ' ' + 0 + ' ' + ArkXIn + ' ' + ArkYIn + ' ';
            const close = 'Z';

            paths[i] =  move + lineOut + arcOut + lineIn + arcIn + close;
            startDegree = finishDegree;
            finishDegree = startDegree + 360 / steps;

        }
        return paths;
    }

    onMouseenterHandler(num: number, target: string) {
        this.resetColor(target);
        if (target === 'hour' && this.onMousedown) {
            this.addHour(num);
            this.enterCellColor[num] = this._enterColor;
        } else if (target === 'min' && this.onMousedown) {
            this.addMinits(num);
            this.enterMinCellColor[num] = this._enterColor;
        }

    }
    onMouseleaveHandler(num: number, target: string) {
        this.resetColor(target);
    }

    onMousedownHandler(num: number, target: string) {
        this.onMousedown = true;
        if (target === 'hour') {
            this.addHour(num);
            this.enterCellColor[num] = this._enterColor;
        } else if (target === 'min') {
            this.addMinits(num);
            this.enterMinCellColor[num] = this._enterColor;
        }

    }
    onMouseupHandler(num: number, target: string) {
        if (this.onMousedown) {
            this.onMousedown = false;
            this.resetColor(target);

            if (this._showHour) {
                this._showHour = false;
                this._showMinits = true;                }
            if (target === 'min') {
                // this operation is goback to return page
                // this.location.back();

                // this operation is send back to shared service
                const clockData = {hour: this.setHour, min: this.setMinits};
                this.subjectsService.publish('clock-data', clockData);
            }
        }
    }
    onSwitch(): void {
        if (this._showMinits) {
            this._showHour = true;
            this._showMinits = false;
        } else {
            this._showHour = false;
            this._showMinits = true;
        }
    }

    addHour(num: number): void {
        this.setHour = num;
    }
    addMinits(num: number): void {
        this.setMinits = num;
    }
    resetColor(target: string): void {
        if (target === 'hour') {
            for (const key in this.enterCellColor) {
                if (this.enterCellColor.hasOwnProperty(key)) {
                    this.enterCellColor[key] = this._baseColor;
                }
            }
        } else if (target === 'min') {
            for (const key in this.enterMinCellColor) {
                if (this.enterMinCellColor.hasOwnProperty(key)) {
                    this.enterMinCellColor[key] = this._baseColor;
                }
            }
        }
    }
    @HostListener('document:keyup', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        const x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        // this.router.navigate(['/admin']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
