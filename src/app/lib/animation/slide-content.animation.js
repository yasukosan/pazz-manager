"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var animations_1 = require("@angular/animations");
exports.slideContentAnimation = animations_1.trigger('slideStatus', [
    // end state styles for route container (host)
    animations_1.state('*', animations_1.style({
        // the view covers the whole screen with a semi tranparent background
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.8)'
    })),
    // route 'enter' transition
    animations_1.transition('* => show', [
        animations_1.style({
            right: '-2000px',
            backgroundColor: 'rgba(0, 0, 0, 0)'
        }),
        animations_1.animate('.5s ease-in-out', animations_1.style({
            right: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.8)'
        }))
    ]),
    // route 'leave' transition
    animations_1.transition('* => hide', [
        animations_1.animate('.5s ease-in-out', animations_1.style({
            right: '-2000px',
            backgroundColor: 'rgba(0, 0, 0, 0)'
        }))
    ])
]);
//# sourceMappingURL=slide-content.animation.js.map