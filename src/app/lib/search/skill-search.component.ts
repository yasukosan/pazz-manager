// import { Component, OnInit, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
// import { Subscription } from 'rxjs/Subscription';
import { Skill, SkillService } from '../../service/index';
import { LocalDataService } from '../../service/local-data.service';

@Component({
    selector: 'skill-search',
    templateUrl: `./skill-search.component.html`,
    styleUrls: ['./search.scss']
})

export class SkillSearchComponent {

    // @Output() setSkill = new EventEmitter<Skill>();

    // private subscription: Subscription;

    public skill: Skill = new Skill();

    skills: Skill[];
    searchSkill: string;
    skillList: Skill[];
    searchOn: boolean = true;

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private skillservice: SkillService
    ) {
        this.skillservice
            .getSkills()
            .then((skills: Skill[]) => this.skills = skills);
    }

    selectSkill(skill: number): void {
        console.log('Chiled::' + this.skills[skill].Name);
        this.localDataService.sendToParent(this.skills[skill]);
        // this.setSkill.emit(this.skills[skill]);
    }
    search(): void {
        if (this.searchOn) {
            this.searchOn = false;
            setTimeout(() => {
                this.doSearch();
                this.searchOn = true;
            }, 500);
        }
    }
    doSearch(): void {
        console.log(this.searchSkill);
        if (this.searchSkill !== undefined) {
            this.skillList = new Array(0);
            for (const skill in this.skills) {
                if (this.skills.hasOwnProperty(skill)) {
                    let searchId: String = this.skills[skill].id.toString();
                    if (this.skills[skill].Name.indexOf(this.searchSkill) > 0
                        || searchId.indexOf(this.searchSkill)  > 0) {
                            this.skillList.push(this.skills[skill]);
                        }
                }
            };
        }
    }
}
