import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Monster, MonsterService } from '../../service/index';
import { LocalDataService } from '../../service/local-data.service';

@Component({
    selector: 'monster-search',
    templateUrl: './monster-search.component.html',
    styleUrls: ['./search.scss']
})

export class MonsterSearchComponent {

    public monster: Monster = new Monster();

    monsters: Monster[];
    searchMonster: string;
    monsterList: Monster[];
    searchOn: boolean = true;

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private monsterService: MonsterService
    ) {
        this.monsterService
            .getMonsters()
            .then((monsters: Monster[]) => this.monsters = monsters);
    }

    selectMonster(monster: number): void {
        const id = monster - 1;
        console.log('Chiled::' + this.monsters[id].Name);
        this.localDataService.sendToParent(this.monsters[id]);
    }

    search(): void {
        if (this.searchOn) {
            this.searchOn = false;
            setTimeout(() => {
                this.doSearch();
                this.searchOn = true;
            }, 500);
        }
    }
    doSearch(): void {
        console.log(this.searchMonster);

        if (this.searchMonster !== undefined) {
            this.monsterList = new Array(0);
            for (const monster in this.monsters) {
                if (this.monsters.hasOwnProperty(monster)) {
                    let searchId: string = this.monsters[monster].id.toString();
                    if (this.monsters[monster].Name.indexOf(this.searchMonster) > 0
                        || searchId.indexOf(this.searchMonster) > 0) {
                            this.monsterList.push(this.monsters[monster]);
                        }
                }
            }
        }
    }
}
