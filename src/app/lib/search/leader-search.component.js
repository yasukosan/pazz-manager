"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var index_1 = require("../../service/index");
var local_data_service_1 = require("../../service/local-data.service");
var LeaderSearchComponent = (function () {
    function LeaderSearchComponent(router, location, localDataService, leaderservice) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.leaderservice = leaderservice;
        this.leader = new index_1.Leader();
        this.searchOn = true;
        this.onAlt = false;
        this.leaderservice
            .getLeaders()
            .then(function (leaders) { return _this.leaders = leaders; });
    }
    LeaderSearchComponent.prototype.selectLeader = function (leader) {
        console.log('Chiled::' + this.leaders[leader].Name);
        this.localDataService.sendToParent(this.leaders[leader]);
    };
    LeaderSearchComponent.prototype.closeLeader = function () {
        this.localDataService.sendToParent('close');
    };
    LeaderSearchComponent.prototype.search = function () {
        var _this = this;
        if (this.searchOn) {
            this.searchOn = false;
            setTimeout(function () {
                _this.doSearch();
                _this.searchOn = true;
            }, 500);
        }
    };
    LeaderSearchComponent.prototype.doSearch = function () {
        console.log(this.searchLeader);
        if (this.searchLeader !== undefined) {
            this.leaderList = new Array(0);
            for (var leader in this.leaders) {
                if (this.leaders.hasOwnProperty(leader)) {
                    var searchId = this.leaders[leader].id.toString();
                    if (this.leaders[leader].Name.indexOf(this.searchLeader) > 0
                        || searchId.indexOf(this.searchLeader) > 0) {
                        this.leaderList.push(this.leaders[leader]);
                    }
                }
            }
        }
    };
    LeaderSearchComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 8 || x === 27 || this.onAlt && x === 37) {
            return this.refreshNavigation();
        }
        if (x === 18) {
            this.onAlt = true;
        }
    };
    LeaderSearchComponent.prototype.refreshNavigation = function () {
        this.closeLeader();
        return false;
    };
    LeaderSearchComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onAlt = (x === 18) ? false : false;
    };
    return LeaderSearchComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], LeaderSearchComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], LeaderSearchComponent.prototype, "onKeyupHandler", null);
LeaderSearchComponent = __decorate([
    core_1.Component({
        selector: 'leader-search',
        templateUrl: './leader-search.component.html',
        styleUrls: ['./search.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        index_1.LeaderService])
], LeaderSearchComponent);
exports.LeaderSearchComponent = LeaderSearchComponent;
//# sourceMappingURL=leader-search.component.js.map