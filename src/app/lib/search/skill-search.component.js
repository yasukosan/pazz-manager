"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { Component, OnInit, OnDestroy } from '@angular/core';
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
// import { Subscription } from 'rxjs/Subscription';
var index_1 = require("../../service/index");
var local_data_service_1 = require("../../service/local-data.service");
var SkillSearchComponent = (function () {
    function SkillSearchComponent(router, location, localDataService, skillservice) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.skillservice = skillservice;
        // @Output() setSkill = new EventEmitter<Skill>();
        // private subscription: Subscription;
        this.skill = new index_1.Skill();
        this.searchOn = true;
        this.skillservice
            .getSkills()
            .then(function (skills) { return _this.skills = skills; });
    }
    SkillSearchComponent.prototype.selectSkill = function (skill) {
        console.log('Chiled::' + this.skills[skill].Name);
        this.localDataService.sendToParent(this.skills[skill]);
        // this.setSkill.emit(this.skills[skill]);
    };
    SkillSearchComponent.prototype.search = function () {
        var _this = this;
        if (this.searchOn) {
            this.searchOn = false;
            setTimeout(function () {
                _this.doSearch();
                _this.searchOn = true;
            }, 500);
        }
    };
    SkillSearchComponent.prototype.doSearch = function () {
        console.log(this.searchSkill);
        if (this.searchSkill !== undefined) {
            this.skillList = new Array(0);
            for (var skill in this.skills) {
                if (this.skills.hasOwnProperty(skill)) {
                    var searchId = this.skills[skill].id.toString();
                    if (this.skills[skill].Name.indexOf(this.searchSkill) > 0
                        || searchId.indexOf(this.searchSkill) > 0) {
                        this.skillList.push(this.skills[skill]);
                    }
                }
            }
            ;
        }
    };
    return SkillSearchComponent;
}());
SkillSearchComponent = __decorate([
    core_1.Component({
        selector: 'skill-search',
        templateUrl: "./skill-search.component.html",
        styleUrls: ['./search.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        index_1.SkillService])
], SkillSearchComponent);
exports.SkillSearchComponent = SkillSearchComponent;
//# sourceMappingURL=skill-search.component.js.map