"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./skill.component"));
__export(require("./skill-add.component"));
__export(require("./skill-detail.component"));
//# sourceMappingURL=index.js.map