import { Component, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
// Import Service
import { Skill, SkillService } from '../service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation Component
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';

@Component({
    selector: 'skill-add',
    templateUrl: './skill-add.component.html',
    animations: [slideInOutAnimation],
    host: { '[@slideInOutAnimation]': '' }
})

export class SkillAddComponent {

    public skill: Skill = new Skill();
    onCtrl: boolean = false;

    constructor(
        private router: Router,
        private skillService: SkillService,
        private location: Location,
        private localDataService: LocalDataService,
        private subjectsService: SubjectsService
    ) {}

    add(): void {

        if (!this.skill.id && !this.skill.Name) { return; }
        this.skillService.create(name)
            .then(() => {
                this.subjectsService.publish('skill-updated');
                // console.log('new skill add success');
                this.goBack();
            });
    }
    goBack(): void {
        this.location.back();
    }
    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
