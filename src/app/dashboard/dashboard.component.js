"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../service/index");
var index_2 = require("../service/index");
var fade_in_animation_1 = require("../lib/animation/fade-in.animation");
var DashboardComponent = (function () {
    function DashboardComponent(monsterService, skillService, leaderService, arousalService) {
        this.monsterService = monsterService;
        this.skillService = skillService;
        this.leaderService = leaderService;
        this.arousalService = arousalService;
        this.monsters = [];
        this.skills = [];
        this.leaders = [];
        this.arousals = [];
        this.counts = new Array(0);
        this.start = 0;
        this.len = 10;
        Hello();
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.getMonster();
        this.getSkill();
        this.getLeader();
        this.getArousal();
    };
    DashboardComponent.prototype.getMonster = function () {
        var _this = this;
        this.monsterService.getMonsters()
            .then(function (monsters) { return _this.monsters = monsters; })
            .then(function (monsters) { return _this.counts = new Array(Math.ceil(monsters.length / 10)); });
    };
    DashboardComponent.prototype.getSkill = function () {
        var _this = this;
        this.skillService.getSkills()
            .then(function (skills) { return _this.skills = skills; });
    };
    DashboardComponent.prototype.getLeader = function () {
        var _this = this;
        this.leaderService.getLeaders()
            .then(function (leaders) { return _this.leaders = leaders; });
    };
    DashboardComponent.prototype.getArousal = function () {
        var _this = this;
        this.arousalService.getArousals()
            .then(function (arousals) { return _this.arousals = arousals; });
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    core_1.Component({
        selector: 'pazz-dashboard',
        templateUrl: "./dashboard.component.html",
        animations: [fade_in_animation_1.fadeInAnimation]
    }),
    __metadata("design:paramtypes", [index_1.MonsterService,
        index_1.SkillService,
        index_2.LeaderService,
        index_2.ArousalService])
], DashboardComponent);
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map