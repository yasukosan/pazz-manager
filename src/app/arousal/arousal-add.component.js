"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
// Import Service
var index_1 = require("../service/index");
// Import Local Share Service
// import { LocalDataService } from '../service/local-data.service';
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var ArousalAddComponent = (function () {
    function ArousalAddComponent(router, arousalService, location, 
        // private localDataService: LocalDataService,
        subjectsService) {
        this.router = router;
        this.arousalService = arousalService;
        this.location = location;
        this.subjectsService = subjectsService;
        this.arousal = new index_1.Arousal();
        this.onCtrl = false;
    }
    ArousalAddComponent.prototype.add = function () {
        var _this = this;
        if (!this.arousal.id && this.arousal.Name) {
            return;
        }
        this.arousalService.create(this.arousal)
            .then(function () {
            _this.subjectsService.publish('leader-updated');
            _this.goBack();
        });
    };
    ArousalAddComponent.prototype.goBack = function () {
        this.location.back();
    };
    ArousalAddComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    ArousalAddComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    ArousalAddComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return ArousalAddComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], ArousalAddComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], ArousalAddComponent.prototype, "onKeyupHandler", null);
ArousalAddComponent = __decorate([
    core_1.Component({
        selector: 'arousal-add',
        templateUrl: './arousal-add.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.ArousalService,
        common_1.Location,
        subjects_service_1.SubjectsService])
], ArousalAddComponent);
exports.ArousalAddComponent = ArousalAddComponent;
//# sourceMappingURL=arousal-add.component.js.map