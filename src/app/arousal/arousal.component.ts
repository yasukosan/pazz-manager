import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
// Import Service
import { Arousal, ArousalService } from '../service/index';
// Import TherdParty Service
import { CsvMakerService } from '../lib_service/index';
// Import Local Share Service
// import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from  '../service/subjects.service';

@Component({
    selector: 'pazz-arousal',
    templateUrl: './arousal.component.html'
})

export class ArousalComponent implements OnInit {
    navigate: boolean = true;
    arousals: Arousal[];
    selectedArousal: Arousal;

    counts: number[] = new Array(0);
    start = 0;
    len = 10;

    subscription: Subscription;

    constructor(
        private router: Router,
        private arousalService: ArousalService,
        // private localdataService: LocalDataService,
        private subjectsService: SubjectsService,
        private csvmakerService: CsvMakerService
    ) {}
    ngOnInit(): void {
        this.getArousals();
        this.reloadCheck();
    }
    pager(page: number) {
        this.start = this.len * page;
    }
    reloadCheck(): void {
        this.subscription = this.subjectsService
            .on('arousal-updated')
            .subscribe(() => {
                console.log('Do update Arousals');
                this.getArousals();
            });
    }

    getArousals(): void {
        this.arousalService
            .getArousals()
            .then((arousal: Arousal[]) => this.arousals = arousal)
            .then((arousal: Arousal[]) => this.counts = new Array(Math.ceil(arousal.length / 10)) );
    }

    delete(arousal: Arousal): void {
        this.arousalService
            .delete(arousal.id)
            .then(() => {
                this.arousals = this.arousals
                    .filter((_arousal: Arousal) => _arousal !== arousal);
                if (this.selectedArousal === arousal) {
                    this.selectedArousal = null;
                }
            });
    }

    gotoDetail(): void {
        this.router.navigate(['./detail-a', this.selectedArousal.id]);
    }

    onSelect(arousal: Arousal): void {
        this.selectedArousal = arousal;
        console.log(this.selectedArousal);
    }
    getcsvFile(): void {
        this.csvmakerService.setCsvName('arousal_list');
        let data = this.csvmakerService.convObjectToCSV(this.arousals);
        this.csvmakerService.getCsv(data);
    }
}
