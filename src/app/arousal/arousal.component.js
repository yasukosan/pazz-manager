"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Import Service
var index_1 = require("../service/index");
// Import TherdParty Service
var index_2 = require("../lib_service/index");
// Import Local Share Service
// import { LocalDataService } from '../service/local-data.service';
var subjects_service_1 = require("../service/subjects.service");
var ArousalComponent = (function () {
    function ArousalComponent(router, arousalService, 
        // private localdataService: LocalDataService,
        subjectsService, csvmakerService) {
        this.router = router;
        this.arousalService = arousalService;
        this.subjectsService = subjectsService;
        this.csvmakerService = csvmakerService;
        this.navigate = true;
        this.counts = new Array(0);
        this.start = 0;
        this.len = 10;
    }
    ArousalComponent.prototype.ngOnInit = function () {
        this.getArousals();
        this.reloadCheck();
    };
    ArousalComponent.prototype.pager = function (page) {
        this.start = this.len * page;
    };
    ArousalComponent.prototype.reloadCheck = function () {
        var _this = this;
        this.subscription = this.subjectsService
            .on('arousal-updated')
            .subscribe(function () {
            console.log('Do update Arousals');
            _this.getArousals();
        });
    };
    ArousalComponent.prototype.getArousals = function () {
        var _this = this;
        this.arousalService
            .getArousals()
            .then(function (arousal) { return _this.arousals = arousal; })
            .then(function (arousal) { return _this.counts = new Array(Math.ceil(arousal.length / 10)); });
    };
    ArousalComponent.prototype.delete = function (arousal) {
        var _this = this;
        this.arousalService
            .delete(arousal.id)
            .then(function () {
            _this.arousals = _this.arousals
                .filter(function (_arousal) { return _arousal !== arousal; });
            if (_this.selectedArousal === arousal) {
                _this.selectedArousal = null;
            }
        });
    };
    ArousalComponent.prototype.gotoDetail = function () {
        this.router.navigate(['./detail-a', this.selectedArousal.id]);
    };
    ArousalComponent.prototype.onSelect = function (arousal) {
        this.selectedArousal = arousal;
        console.log(this.selectedArousal);
    };
    ArousalComponent.prototype.getcsvFile = function () {
        this.csvmakerService.setCsvName('arousal_list');
        var data = this.csvmakerService.convObjectToCSV(this.arousals);
        this.csvmakerService.getCsv(data);
    };
    return ArousalComponent;
}());
ArousalComponent = __decorate([
    core_1.Component({
        selector: 'pazz-arousal',
        templateUrl: './arousal.component.html'
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.ArousalService,
        subjects_service_1.SubjectsService,
        index_2.CsvMakerService])
], ArousalComponent);
exports.ArousalComponent = ArousalComponent;
//# sourceMappingURL=arousal.component.js.map