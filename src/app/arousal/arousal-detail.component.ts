import 'rxjs/add/operator/switchMap';
import { Component, HostListener , OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
// Import Service
import { Arousal, ArousalService } from '../service/index';
// Import Local Share Service
// import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation Component
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';

@Component({
  selector: 'arousal-detail',
  templateUrl: './arousal-detail.component.html',
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})

export class ArousalDetailComponent implements OnInit {

    arousal: Arousal;
    onCtrl: boolean = false;

    constructor(
        private arousalService: ArousalService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        // private localDataService: LocalDataService,
        private subjectsService: SubjectsService
    ) {}
    ngOnInit(): void {
        this.route.params
        .switchMap((params: Params) => this.arousalService.getArousal(+params['id']))
        .subscribe((arousal: Arousal) => this.arousal = arousal);
    }

    save(): void {
        this.arousalService.update(this.arousal)
            .then(() => {
                this.subjectsService.publish('arousal-updated');
                this.goBack();
            });
    }
    goBack(): void {
        this.location.back();
    }


    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }

    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }

}
