"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./monster.component"));
__export(require("./monster-add.component"));
__export(require("./monster-detail.component"));
//# sourceMappingURL=index.js.map