"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
require("rxjs/add/operator/switchMap");
// Import Searvice
var index_1 = require("../service/index");
var index_2 = require("../service/index");
var index_3 = require("../service/index");
// Import Search Component
var index_4 = require("../lib/search/index");
// Import Select Component
var index_5 = require("../lib/select/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var MonsterDetailComponent = (function () {
    function MonsterDetailComponent(monsterService, skillService, leaderService, arousalService, mtypeService, attributeService, route, router, location, localDataService, subjectsService) {
        this.monsterService = monsterService;
        this.skillService = skillService;
        this.leaderService = leaderService;
        this.arousalService = arousalService;
        this.mtypeService = mtypeService;
        this.attributeService = attributeService;
        this.route = route;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.subjectsService = subjectsService;
        this.subWindow = 'void';
        this.showSkillSearch = false;
        this.showLeaderSearch = false;
        this.searchOn = true;
        this.searchType = 'none';
        this.searchComponents = [
            index_4.SkillSearchComponent,
            index_4.LeaderSearchComponent,
            index_5.TypeSelectComponent,
            index_5.AttributeSelectComponent,
            index_5.ArousalSelectComponent
        ];
        this.subFlag = true;
        this.subTop = '0px';
        this.subLeft = '0px';
        this.onCtrl = false;
        this.debug = true;
    }
    MonsterDetailComponent.prototype.ngOnInit = function () {
        this.getChMsg();
        this.getSkills();
        this.getLeaders();
        this.getMtypes();
        this.getAttributes();
        this.getArousals();
        this.getRouteParams();
    };
    MonsterDetailComponent.prototype.getChMsg = function () {
        var _this = this;
        this.localDataService.toParentData$.subscribe(function (msg) {
            if (_this.searchType === 'skill') {
                _this.pullConsole('Pearent Skill:' + msg.id);
                _this.monster.Skill = msg.id;
            }
            else if (_this.searchType === 'leader') {
                _this.pullConsole('Pearent Leader:' + msg.id);
                _this.monster.Reader = msg.id;
            }
            else if (_this.searchType === 'type') {
                _this.pullConsole(_this.MonsterTypeTarget);
                if (_this.MonsterTypeTarget === 1) {
                    _this.monster.Type = msg.id;
                }
                else if (_this.MonsterTypeTarget === 2) {
                    _this.monster.SubType = msg.id;
                }
                else if (_this.MonsterTypeTarget === 3) {
                    _this.monster.ThirdType = msg.id;
                }
            }
            else if (_this.searchType === 'attribute') {
                if (_this.MonsterAttributeTarget === 1) {
                    _this.monster.Attribute = msg.id;
                }
                else if (_this.MonsterAttributeTarget === 2) {
                    _this.monster.SubAttribute = msg.id;
                }
            }
            else if (_this.searchType === 'arousal') {
            }
            _this.closeSearch();
        });
    };
    /**
     * 情報取得
     */
    MonsterDetailComponent.prototype.getRouteParams = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.monsterService.getMonster(+(params['id'] - 1)); })
            .subscribe(function (monster) { return _this.monster = monster; });
    };
    MonsterDetailComponent.prototype.getSkills = function () {
        var _this = this;
        this.skillService
            .getSkills()
            .then(function (skills) { return _this.skills = skills; });
    };
    MonsterDetailComponent.prototype.getLeaders = function () {
        var _this = this;
        this.leaderService
            .getLeaders()
            .then(function (leaders) { return _this.leaders = leaders; });
    };
    MonsterDetailComponent.prototype.getArousals = function () {
        var _this = this;
        this.arousalService
            .getArousals()
            .then(function (arousals) { return _this.arousals = arousals; });
    };
    MonsterDetailComponent.prototype.getMtypes = function () {
        var _this = this;
        this.mtypeService
            .getMtypes()
            .then(function (mtypes) { return _this.mtypes = mtypes; });
    };
    MonsterDetailComponent.prototype.getAttributes = function () {
        var _this = this;
        this.attributeService
            .getAttributes()
            .then(function (attributes) { return _this.attributes = attributes; });
    };
    /**
     * 検索処理
     */
    MonsterDetailComponent.prototype.closeSearch = function () {
        this.subWindow = 'hide';
        this.subFlag = true;
        this.subTop = '';
        this.subLeft = '';
    };
    MonsterDetailComponent.prototype.showSearchSkill = function (e) {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[0];
            this.localDataService.sendToChildSkill(this.skills);
            this.setSearchParam('40%', '20%', 'skill');
        }
    };
    MonsterDetailComponent.prototype.showSearchLeader = function (e) {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[1];
            this.localDataService.sendToChildLeader(this.leaders);
            this.setSearchParam('40%', '20%', 'leader');
        }
    };
    MonsterDetailComponent.prototype.setSearchParam = function (top, left, type) {
        this.subWindow = 'show';
        this.subFlag = false;
        this.subTop = top;
        this.subLeft = left;
        this.searchType = type;
    };
    MonsterDetailComponent.prototype.setSearch = function (data) {
        console.log('Chiled Event Get::' + data.Name);
    };
    /**
     * リスト選択
     */
    MonsterDetailComponent.prototype.showSelectType = function (tag) {
        if (this.subFlag) {
            this.MonsterTypeTarget = tag;
            this.pullConsole('type tag ' + tag);
            this.searchContent = this.searchComponents[2];
            this.setSearchParam('40%', '20%', 'type');
        }
    };
    MonsterDetailComponent.prototype.showSelectAttribute = function (tag) {
        if (this.subFlag) {
            this.MonsterAttributeTarget = tag;
            this.pullConsole('attribute tag ' + tag);
            this.searchContent = this.searchComponents[3];
            this.setSearchParam('40%', '20%', 'attribute');
        }
    };
    MonsterDetailComponent.prototype.showSelectArousal = function () {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[4];
            this.setSearchParam('40%', '20%', 'arousal');
        }
    };
    MonsterDetailComponent.prototype.save = function () {
        var _this = this;
        this.monsterService.update(this.monster)
            .then(function () {
            _this.subjectsService.publish('monster-updated', 'test');
            _this.goBack();
        });
    };
    MonsterDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    MonsterDetailComponent.prototype.pullConsole = function (mess) {
        if (this.debug) {
            console.log(mess);
        }
    };
    MonsterDetailComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    MonsterDetailComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    MonsterDetailComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return MonsterDetailComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], MonsterDetailComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], MonsterDetailComponent.prototype, "onKeyupHandler", null);
MonsterDetailComponent = __decorate([
    core_1.Component({
        selector: 'monster-detail',
        templateUrl: './monster-detail.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation, slide_content_animation_1.slideContentAnimation],
        providers: [local_data_service_1.LocalDataService],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [index_1.MonsterService,
        index_2.SkillService,
        index_2.LeaderService,
        index_1.ArousalService,
        index_3.MtypeService,
        index_3.AttributeService,
        router_1.ActivatedRoute,
        router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService])
], MonsterDetailComponent);
exports.MonsterDetailComponent = MonsterDetailComponent;
//# sourceMappingURL=monster-detail.component.js.map