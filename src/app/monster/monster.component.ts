import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Import Main Component
import { MonsterSearchComponent } from '../lib/search/monster-search.component';
import { PdfPrintComponent } from '../lib/index';
import { ImageLoaderComponent, ImagePazzComponent } from '../lib/image_module/index';

// Import Service
import { Monster, MonsterService, Arousal, ArousalService } from '../service/index';
import { Skill, SkillService, Leader, LeaderService } from '../service/index';
import { Mtype, MtypeService, Attribute, AttributeService } from '../service/index';
// Import TherdParty Service
import { CsvMakerService, PdfMakerService } from '../lib_service/index';
import { ListLayoutService, RecruiteLayoutService } from '../lib_service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation
import { fadeInAnimation } from '../lib/animation/fade-in.animation';
import { slideContentAnimation } from '../lib/animation/slide-content.animation';

@Component({
    selector: 'pazz-monsters',
    templateUrl: './monster.component.html',
    providers: [ LocalDataService ],
    animations: [ fadeInAnimation, slideContentAnimation ],
    host: { '[@fadeInAnimation]': '' }
})

export class MonsterComponent implements OnInit, OnDestroy {
    navigate: boolean = true;
    showMonsters: Monster[] = new Array(0);
    monsters: Monster[];
    skills: Skill[];
    leaders: Leader[];
    arousals: Arousal[];
    mtypes: Mtype[];
    attributes: Attribute[];
    selectedMonster: Monster;

    counts: number[] = new Array(0);
    start = 0;
    len = 10;

    subFlag: boolean = true;
    subWindow: string = 'void';
    searchContent: any;

    subscription: Subscription;

    constructor(
        private router: Router,
        private monsterService: MonsterService,
        private skillService: SkillService,
        private leaderService: LeaderService,
        private arousalService: ArousalService,
        private mtypeService: MtypeService,
        private attributeService: AttributeService,
        private localdataService: LocalDataService,
        private subjectsService: SubjectsService,
        private csvmakerService: CsvMakerService,
        private pdfmakerService: PdfMakerService,
        private listlayoutService: ListLayoutService,
        private recruitelayoutService: RecruiteLayoutService
    ) {}

    ngOnInit(): void {
        this.getArousals();
        this.getAttributes();
        this.getSkills();
        this.getLeaders();
        this.getMtypes();
        this.getMonsters();

        this.reloadCheck();
        this.getMsg();

        this.closeSubWindowCheck();
    }
    closeSubWindowCheck(): void {
        this.subjectsService
            .on('closer')
            .subscribe(() => {
                this.closeSearch();
            });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    pager(page: number) {
        this.start = this.len * page;
        this.showMonsters = [];
        for (let i = this.start; i < (this.len + this.start); i++) {
            this.showMonsters.push(this.monsters[i]);
        }
    }
    getMsg(): any {
        this.localdataService.toParentData$.subscribe(
            msg => {
                console.log(msg.Name);
                this.closeSearch();
                this.router.navigate(['/monster/detail/', msg.id]);
            }
        );
    }
    reloadCheck(): void {
        this.subscription = this.subjectsService
            .on('monster-updated')
            .subscribe(msg => {
                console.log('Do update Monsters' + msg);
                this.getMonsters();
            });
    }
    getMonsters(): void {
        this.monsterService
            .getMonsters()
            .then((monsters: Monster[]) => this.monsters = monsters)
            .then((monsters: Monster[]) => this.counts = new Array(Math.ceil(monsters.length / 10)) );
    }

    getSkills(): void {
        this.skillService.getSkills()
        .then((skills: Skill[]) => this.skills = skills); }
    getLeaders(): void {
        this.leaderService.getLeaders()
        .then((leaders: Leader[]) => this.leaders = leaders); }
    getArousals(): void {
        this.arousalService.getArousals()
        .then((arousals: Arousal[]) => this.arousals = arousals); }
    getMtypes(): void {
        this.mtypeService.getMtypes()
        .then((mtypes: Mtype[]) => this.mtypes = mtypes); }
    getAttributes(): void {
        this.attributeService.getAttributes()
        .then((attributes: Attribute[]) => this.attributes = attributes); }

    delete(monster: Monster): void {
        this.monsterService
            .delete(monster.id)
            .then(() => {
                this.monsters = this.monsters
                    .filter((_monster: Monster) => _monster !== monster);
                if (this.selectedMonster === monster) {
                    this.selectedMonster = null;
                }
            });
    }

    showSearch(): void {
        this.searchContent = MonsterSearchComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    }
    closeSearch(): void {
        this.subFlag = true;
        this.subWindow = 'hide';
    }
    gotoDetail(): void {
        this.router.navigate(['./detail-m', this.selectedMonster.id]);
    }

    onSelect(monster: Monster): void {
        this.selectedMonster = monster;
        console.log(this.selectedMonster);
    }

    getcsvFile(): void {
        this.csvmakerService.setCsvName('monster_list');
        const data = this.csvmakerService.convObjectToCSV(this.monsters);
        this.csvmakerService.getCsv(data);
    }
    getPdfFile(): void {
        // const dd = this.listlayoutService.makePdfLayout(this.monsters);
        const dd = this.recruitelayoutService.makePdfLayout();
        console.log(dd);
        const pdf = this.pdfmakerService.testPdfMake(dd);
        pdf.print();
    }

    showPdfPrint(): void {
        this.searchContent = ImageLoaderComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    }
    showMonsterMaker(): void {
        this.searchContent = ImagePazzComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    }
}
