"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
// Import Service
var index_1 = require("../service/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var LeaderAddComponent = (function () {
    function LeaderAddComponent(router, leaderService, location, localDataService, subjectsService) {
        this.router = router;
        this.leaderService = leaderService;
        this.location = location;
        this.localDataService = localDataService;
        this.subjectsService = subjectsService;
        this.leader = new index_1.Leader();
        this.onCtrl = false;
    }
    LeaderAddComponent.prototype.add = function () {
        var _this = this;
        if (!this.leader.id && !this.leader.Name) {
            return;
        }
        var state = this.leaderService.create(this.leader)
            .then(function () {
            _this.subjectsService.publish('leader-updated');
            // console.log('new leader skill add success');
            _this.goBack();
        });
        if (state) {
        }
    };
    LeaderAddComponent.prototype.goBack = function () {
        this.location.back();
    };
    LeaderAddComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116) {
            return this.refreshNavigation();
        }
        else if (this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    LeaderAddComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    LeaderAddComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return LeaderAddComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], LeaderAddComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], LeaderAddComponent.prototype, "onKeyupHandler", null);
LeaderAddComponent = __decorate([
    core_1.Component({
        selector: 'leader-add',
        templateUrl: './leader-add.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.LeaderService,
        common_1.Location,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService])
], LeaderAddComponent);
exports.LeaderAddComponent = LeaderAddComponent;
//# sourceMappingURL=leader-add.component.js.map