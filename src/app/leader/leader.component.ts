import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
// Import Main Component
import { LeaderSearchComponent } from '../lib/search/leader-search.component';
// Import Service
import { Leader, LeaderService } from '../service/index';
// Import TherdParty Service
import { CsvMakerService } from '../lib_service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from  '../service/subjects.service';
// Import Animation
import { slideContentAnimation } from '../lib/animation/slide-content.animation';

@Component({
    selector: 'pazz-leader',
    templateUrl: './leader.component.html',
    providers: [ LocalDataService ],
    animations: [ slideContentAnimation ]
})

export class LeaderComponent implements OnInit {
    navigate: boolean = true;
    leaders: Leader[];
    selectedLeader: Leader;

    counts: number[] = new Array(0);
    start = 0;
    len = 10;

    subFlag: boolean = true;
    subWindow: string = 'void';
    searchContent: any;

    subscription: Subscription;

    constructor(
        private router: Router,
        private leaderService: LeaderService,
        private localdataService: LocalDataService,
        private subjectsService: SubjectsService,
        private csvmakerService: CsvMakerService
    ) {}

    ngOnInit(): void {
        this.getLeaders();
        this.reloadCheck();
        this.getMsg();
    }

    pager(page: number) {
        this.start = this.len * page;
    }
    getMsg(): any {
        this.localdataService.toParentData$.subscribe(
            msg => {
                console.log(msg.Name);
                this.closeSearch();
                this.router.navigate(['/leader/detail/', msg.id]);
            }
        );
    }
    reloadCheck(): void {
        this.subscription = this.subjectsService
            .on('leader-updated')
            .subscribe(() => {
                console.log('Do update Leaders');
                this.getLeaders();
            });
    }
    getLeaders(): void {
        this.leaderService
            .getLeaders()
            .then((leaders: Leader[]) => this.leaders = leaders)
            .then((leaders: Leader[]) => this.counts = new Array(Math.ceil(leaders.length / 10)) );
    }

    delete(leader: Leader): void {
        this.leaderService
            .delete(leader.id)
            .then(() => {
                this.leaders = this.leaders
                    .filter((_leader: Leader) => _leader !== leader);
                if (this.selectedLeader === leader) {
                    this.selectedLeader = null;
                }
            });
    }
    showSearch(): void {
        this.searchContent = LeaderSearchComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    }
    closeSearch(): void {
        this.subFlag = true;
        this.subWindow = 'hide';
    }

    gotoDetail(): void {
        this.router.navigate(['./detail-l', this.selectedLeader.id]);
    }

    onSelect(leader: Leader): void {
        this.selectedLeader = leader;
        console.log(this.selectedLeader);
    }

    getcsvFile(): void {
        this.csvmakerService.setCsvName('leader_skill_list');
        let data = this.csvmakerService.convObjectToCSV(this.leaders);
        this.csvmakerService.getCsv(data);
    }
}
