"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Import Main Component
var leader_search_component_1 = require("../lib/search/leader-search.component");
// Import Service
var index_1 = require("../service/index");
// Import TherdParty Service
var index_2 = require("../lib_service/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var LeaderComponent = (function () {
    function LeaderComponent(router, leaderService, localdataService, subjectsService, csvmakerService) {
        this.router = router;
        this.leaderService = leaderService;
        this.localdataService = localdataService;
        this.subjectsService = subjectsService;
        this.csvmakerService = csvmakerService;
        this.navigate = true;
        this.counts = new Array(0);
        this.start = 0;
        this.len = 10;
        this.subFlag = true;
        this.subWindow = 'void';
    }
    LeaderComponent.prototype.ngOnInit = function () {
        this.getLeaders();
        this.reloadCheck();
        this.getMsg();
    };
    LeaderComponent.prototype.pager = function (page) {
        this.start = this.len * page;
    };
    LeaderComponent.prototype.getMsg = function () {
        var _this = this;
        this.localdataService.toParentData$.subscribe(function (msg) {
            console.log(msg.Name);
            _this.closeSearch();
            _this.router.navigate(['/leader/detail/', msg.id]);
        });
    };
    LeaderComponent.prototype.reloadCheck = function () {
        var _this = this;
        this.subscription = this.subjectsService
            .on('leader-updated')
            .subscribe(function () {
            console.log('Do update Leaders');
            _this.getLeaders();
        });
    };
    LeaderComponent.prototype.getLeaders = function () {
        var _this = this;
        this.leaderService
            .getLeaders()
            .then(function (leaders) { return _this.leaders = leaders; })
            .then(function (leaders) { return _this.counts = new Array(Math.ceil(leaders.length / 10)); });
    };
    LeaderComponent.prototype.delete = function (leader) {
        var _this = this;
        this.leaderService
            .delete(leader.id)
            .then(function () {
            _this.leaders = _this.leaders
                .filter(function (_leader) { return _leader !== leader; });
            if (_this.selectedLeader === leader) {
                _this.selectedLeader = null;
            }
        });
    };
    LeaderComponent.prototype.showSearch = function () {
        this.searchContent = leader_search_component_1.LeaderSearchComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    };
    LeaderComponent.prototype.closeSearch = function () {
        this.subFlag = true;
        this.subWindow = 'hide';
    };
    LeaderComponent.prototype.gotoDetail = function () {
        this.router.navigate(['./detail-l', this.selectedLeader.id]);
    };
    LeaderComponent.prototype.onSelect = function (leader) {
        this.selectedLeader = leader;
        console.log(this.selectedLeader);
    };
    LeaderComponent.prototype.getcsvFile = function () {
        this.csvmakerService.setCsvName('leader_skill_list');
        var data = this.csvmakerService.convObjectToCSV(this.leaders);
        this.csvmakerService.getCsv(data);
    };
    return LeaderComponent;
}());
LeaderComponent = __decorate([
    core_1.Component({
        selector: 'pazz-leader',
        templateUrl: './leader.component.html',
        providers: [local_data_service_1.LocalDataService],
        animations: [slide_content_animation_1.slideContentAnimation]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.LeaderService,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService,
        index_2.CsvMakerService])
], LeaderComponent);
exports.LeaderComponent = LeaderComponent;
//# sourceMappingURL=leader.component.js.map