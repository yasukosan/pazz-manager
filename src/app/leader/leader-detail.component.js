"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/switchMap");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var leader_service_1 = require("../service/leader.service");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var LeaderDetailComponent = (function () {
    function LeaderDetailComponent(leaderService, route, router, location, localDataService, subjectsService) {
        this.leaderService = leaderService;
        this.route = route;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.subjectsService = subjectsService;
        this.onCtrl = false;
    }
    LeaderDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.leaderService.getLeader(+params['id']); })
            .subscribe(function (leader) { return _this.leader = leader; });
    };
    LeaderDetailComponent.prototype.save = function () {
        var _this = this;
        this.leaderService.update(this.leader)
            .then(function () {
            _this.subjectsService.publish('leader-updated');
            _this.goBack();
        });
    };
    LeaderDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    LeaderDetailComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116) {
            return this.refreshNavigation();
        }
        else if (this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    LeaderDetailComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    LeaderDetailComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return LeaderDetailComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], LeaderDetailComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], LeaderDetailComponent.prototype, "onKeyupHandler", null);
LeaderDetailComponent = __decorate([
    core_1.Component({
        selector: 'leader-detail',
        templateUrl: './leader-detail.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation],
        providers: [local_data_service_1.LocalDataService],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [leader_service_1.LeaderService,
        router_1.ActivatedRoute,
        router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService])
], LeaderDetailComponent);
exports.LeaderDetailComponent = LeaderDetailComponent;
//# sourceMappingURL=leader-detail.component.js.map