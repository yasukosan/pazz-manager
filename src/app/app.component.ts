import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'pazz-manager',
    templateUrl: './app.component.html',
    styleUrls: [
        './css/app.css',
        './css/add.css',
        './css/lists.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    title: string = '管理画面';
}
