import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ClockSelectComponent } from '../lib/select/clock-select.component';
import { slideContentAnimation }  from '../lib/animation/slide-content.animation';
import { SubjectsService } from '../service/subjects.service';
@Component({
    selector: 'pazz-manager',
    templateUrl: './manager.component.html',
    animations: [ slideContentAnimation ]
})

export class ManagerComponent implements OnInit {

    subFlag: boolean = true;
    subWindow: string = 'void';
    subTop: string;
    subLeft: string;
    clockComponent = ClockSelectComponent;

    selectTime: number = 0;

    constructor(
        private subjectService: SubjectsService
    ) {}
    ngOnInit(): void {
        this.getClock();
    }
    getClock(): any {
        this.subjectService.on('clock-data').subscribe(
            msg => {
                console.log(msg.hour + '時' + msg.min + '分');
                this.closeClock();
            }
        );
    }
    showClock($event: Event): void {
        this.subFlag = false;
        this.subWindow = 'show';

    }

    closeClock(): void {
        this.subFlag = true;
        this.subWindow = 'hide';
    }
}
