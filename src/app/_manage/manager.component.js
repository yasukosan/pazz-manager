"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/switchMap");
var core_1 = require("@angular/core");
var clock_select_component_1 = require("../lib/select/clock-select.component");
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var subjects_service_1 = require("../service/subjects.service");
var ManagerComponent = (function () {
    function ManagerComponent(subjectService) {
        this.subjectService = subjectService;
        this.subFlag = true;
        this.subWindow = 'void';
        this.clockComponent = clock_select_component_1.ClockSelectComponent;
        this.selectTime = 0;
    }
    ManagerComponent.prototype.ngOnInit = function () {
        this.getClock();
    };
    ManagerComponent.prototype.getClock = function () {
        var _this = this;
        this.subjectService.on('clock-data').subscribe(function (msg) {
            console.log(msg.hour + '時' + msg.min + '分');
            _this.closeClock();
        });
    };
    ManagerComponent.prototype.showClock = function ($event) {
        this.subFlag = false;
        this.subWindow = 'show';
    };
    ManagerComponent.prototype.closeClock = function () {
        this.subFlag = true;
        this.subWindow = 'hide';
    };
    return ManagerComponent;
}());
ManagerComponent = __decorate([
    core_1.Component({
        selector: 'pazz-manager',
        templateUrl: './manager.component.html',
        animations: [slide_content_animation_1.slideContentAnimation]
    }),
    __metadata("design:paramtypes", [subjects_service_1.SubjectsService])
], ManagerComponent);
exports.ManagerComponent = ManagerComponent;
//# sourceMappingURL=manager.component.js.map