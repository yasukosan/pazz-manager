import { Injectable } from '@angular/core';


@Injectable()
export class ImageResizeService {

    Ratio = 1;

    editTarget;
    editName;

    canvasBase;
    canvasWidth;
    canvasHeight;
    orgImage;

    startPointX;
    startPointY;
    catW;
    catH;

    constructor() {}
    setEdittarget(image): void {
        this.editTarget = image;
    }
    setParam(startPointX, startPointY, catW, catH, ratio): void {
        this.startPointX = startPointX;
        this.startPointY = startPointY;
        this.catW = catW;
        this.catH = catH;
        this.Ratio = ratio;
    }

    /**
     * 画像のトリミング
     * リサイズ処理を行っているので
     * 切り取り位置の指定は縮小率を考慮する必要あり
     */
    catImage(): any {
        const oc = <HTMLCanvasElement> document.createElement('canvas');
        const octx = oc.getContext('2d');

        const img = new Image();
        img.onload = (e) => {
            oc.setAttribute('width', img.naturalWidth.toString());
            oc.setAttribute('height', img.naturalHeight.toString());
            octx.drawImage(
                img, // 切り取りイメージ
                (this.startPointX / this.Ratio),
                (this.startPointY / this.Ratio),
                this.catW / this.Ratio, // 切り取り位置 X座標
                this.catH / this.Ratio, // 切り取り位置 Y座標
                0, 0, this.catW / this.Ratio, this.catH / this.Ratio); // 切り取り後の表示位置とサイズ
            // 切り取り後のイメージを保存
            return oc.toDataURL(this.orgImage['type']);
        };
        img.src = this.orgImage.data;
    }
}

