"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CsvMakerService = (function () {
    function CsvMakerService() {
        this.csvName = 'test';
    }
    CsvMakerService.prototype.setCsvName = function (name) {
        this.csvName = name;
    };
    CsvMakerService.prototype.getCsv = function (data) {
        console.log('start csv maker');
        var csv_array = data;
        var csv_string = '';
        for (var i = 0; i < csv_array.length; i++) {
            csv_string += csv_array[i];
            csv_string += '\r\n';
        }
        // BOM追加
        var bom = new Uint8Array([0xEF, 0xBB, 0xBF]);
        csv_string = '\ufffe' + csv_string;
        // ファイル作成
        var blob = new Blob([bom, csv_string], {
            type: 'text/csv;charset=utf-8;'
        });
        // ダウンロード実行
        if (window.navigator.msSaveOrOpenBlob) {
            // IE
            navigator.msSaveBlob(blob, this.csvName + '.csv');
        }
        else {
            // IE以外(Chrome, Firefox)
            var downloadLink = document.createElement('a');
            downloadLink.setAttribute('href', window.URL.createObjectURL(blob));
            downloadLink.setAttribute('download', this.csvName + '.csv');
            downloadLink.setAttribute('target', '_blank');
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    };
    CsvMakerService.prototype.convObjectToCSV = function (content) {
        var convData = [];
        var _loop_1 = function (key) {
            if (content.hasOwnProperty(key)) {
                var element_1 = content[key];
                convData.push(Object.keys(element_1)
                    .map(function (key) {
                    return element_1[key];
                }));
            }
        };
        for (var key in content) {
            _loop_1(key);
        }
        return convData;
    };
    return CsvMakerService;
}());
CsvMakerService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], CsvMakerService);
exports.CsvMakerService = CsvMakerService;
//# sourceMappingURL=csv-maker.service.js.map