import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { User } from './user';

@Injectable()
export class UserService {

    private serverUrl: string = 'server/index.php';
    private job: string = '_u';
    private detailUser: User;

    private responseStatus: any[] = [];

    constructor(private http: Http) { }

    getAll(): Promise<User[]> {
        let data = {};
        data['job'] = 'get' + this.job;
        data['id'] = 'all';

        return this.http
            .post(
                this.serverUrl,
                JSON.stringify(data),
                this.jwt())
            .toPromise()
            .then(response => {
                return response.json() as User[];
            })
            .catch(this.handleError);
    }

    getById(id: number): Promise<User> {
        /*
        return this.http
            .get('/api/users/' + id,
            this.jwt()).map((response: Response) => response.json());
        */

        let data = {};
        data['job'] = 'get' + this.job;
        data['id'] = `${id}`;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(data),
                this.jwt() )
            .toPromise()
            .then((response: Response) => {
                return response.json() as User;
            })
            .catch(this.handleError);
    }

    create(user: User): Promise<User> {
        // return this.http.post('/api/users', user, this.jwt()).map((response: Response) => response.json());
        const addData: User = user;
        addData['job'] = 'add' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(addData),
                this.jwt() )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
            })
            .catch(this.handleError);
    }

    update(user: User): Promise<User> {
        // return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
        const update: User = user;
        update['job'] = 'update' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(update),
                this.jwt() )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                this.getAll();
            })
            .catch(this.handleError);
    }

    delete(id: number): void {
        // return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    setDetail(user: User): void {
        this.detailUser = user;
    }
    getDetail(): User {
        return this.detailUser;
    }
    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({
                'Authorization': 'Bearer ' + currentUser.token,
                'Content-Type': 'application/json'
            });
            return new RequestOptions({ headers: headers });
        }
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
