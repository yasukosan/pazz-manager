import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

    private serverUrl: string = 'server/index.php';
    private headerJson: Headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http: Http) { }

    login(username: string, password: string) {

        return this.http
            .post(this.serverUrl,
                JSON.stringify({ username: username, password: password, job: 'auth' }),
                { headers: this.headerJson })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    if (user.status === 'success') {
                        localStorage.setItem('currentUser', JSON.stringify(user));
                    }
                }
                return JSON.stringify(user);
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

}
