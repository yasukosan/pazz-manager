"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.serverUrl = 'server/index.php';
        this.job = '_u';
        this.responseStatus = [];
    }
    UserService.prototype.getAll = function () {
        var data = {};
        data['job'] = 'get' + this.job;
        data['id'] = 'all';
        return this.http
            .post(this.serverUrl, JSON.stringify(data), this.jwt())
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getById = function (id) {
        /*
        return this.http
            .get('/api/users/' + id,
            this.jwt()).map((response: Response) => response.json());
        */
        var data = {};
        data['job'] = 'get' + this.job;
        data['id'] = "" + id;
        return this.http
            .post(this.serverUrl, JSON.stringify(data), this.jwt())
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.create = function (user) {
        var _this = this;
        // return this.http.post('/api/users', user, this.jwt()).map((response: Response) => response.json());
        var addData = user;
        addData['job'] = 'add' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(addData), this.jwt())
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.update = function (user) {
        var _this = this;
        // return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
        var update = user;
        update['job'] = 'update' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(update), this.jwt())
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            _this.getAll();
        })
            .catch(this.handleError);
    };
    UserService.prototype.delete = function (id) {
        // return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    };
    UserService.prototype.setDetail = function (user) {
        this.detailUser = user;
    };
    UserService.prototype.getDetail = function () {
        return this.detailUser;
    };
    // private helper methods
    UserService.prototype.jwt = function () {
        // create authorization header with jwt token
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            var headers = new http_1.Headers({
                'Authorization': 'Bearer ' + currentUser.token,
                'Content-Type': 'application/json'
            });
            return new http_1.RequestOptions({ headers: headers });
        }
    };
    UserService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map