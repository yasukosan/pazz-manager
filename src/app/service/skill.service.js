"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
// import { Observable } from 'rxjs';
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
var SkillService = (function () {
    function SkillService(http) {
        this.http = http;
        this.serverUrl = 'server/index.php';
        this.headerJson = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.job = '_s';
        this.skills = new Array();
        this.responseStatus = [];
    }
    SkillService.prototype.getSkills = function () {
        var _this = this;
        if (this.skills.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.skills);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) {
                _this.skills = response.json();
                return response.json();
            })
                .catch(this.handleError);
        }
    };
    SkillService.prototype.getSkill = function (id) {
        var _this = this;
        if (this.skills.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.skills[id]);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = "" + id;
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) { return response.json(); })
                .catch(this.handleError);
        }
    };
    SkillService.prototype.create = function (skill) {
        var _this = this;
        var addData = skill;
        addData['job'] = 'add' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(addData), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            _this.skills[skill.id] = skill;
        })
            .catch(this.handleError);
    };
    SkillService.prototype.update = function (skill) {
        var _this = this;
        var updata = skill;
        updata['job'] = 'update' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(updata), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            for (var key in skill) {
                if (skill.hasOwnProperty(key)) {
                    _this.skills[skill.id][key] = skill[key];
                }
            }
            _this.skills[skill.id] = skill;
        })
            .catch(this.handleError);
    };
    SkillService.prototype.delete = function (id) {
        return this.http
            .delete(this.serverUrl, { headers: this.headerJson })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    SkillService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    return SkillService;
}());
SkillService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], SkillService);
exports.SkillService = SkillService;
//# sourceMappingURL=skill.service.js.map