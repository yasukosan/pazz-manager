import { Injectable } from '@angular/core';
import { Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Mtype } from './mtype';
@Injectable()

export class MtypeService {

    private serverUrl: string = 'server/index.php';
    private headerJson: Headers = new Headers({'Content-Type': 'application/json'});
    private job: string = '_t';

    private mtypes: Mtype[] = new Array();
    private responseStatus: any[] = [];

    constructor(private http: Http) {}

    getMtypes(): Promise<Mtype[]> {
        if (this.mtypes.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.mtypes);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';

            return this.http
                .post(this.serverUrl,
                     JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then(response => {
                    this.mtypes = response.json();
                    return response.json() as Mtype[];
                })
                .catch(this.handleError);
        }

    }

    getMtype(id: number): Promise<Mtype> {
        if (this.mtypes.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.mtypes[id]);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = `${id}`;

            return this.http
                .post(this.serverUrl,
                    JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then((response: Response) => {
                    return response.json() as Mtype;
                })
                .catch(this.handleError);
        }
    }
    create(mtype: Mtype): Promise<Mtype> {
        const addData: Mtype = mtype;
        addData['job'] = 'add' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(addData),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                this.mtypes[mtype.id] = mtype;
            })
            .catch(this.handleError);
    }

    update(mtype: Mtype): Promise<Mtype> {

        const updata: Mtype = mtype;
        updata['job'] = 'update' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(updata),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                for (let key in mtype) {
                    if (mtype.hasOwnProperty(key)) {
                        this.mtypes[mtype.id][key] = mtype[key];
                    }
                }
                this.mtypes[mtype.id] = mtype;
            })
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        return this.http
            .delete(this.serverUrl, {headers: this.headerJson})
            .toPromise()
            .then(() => {
                this.mtypes.splice(id, 1);
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
