import { Injectable } from '@angular/core';
import { Headers, Http, Response} from '@angular/http';
// import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Skill } from './skill';

@Injectable()
export class SkillService {

    private serverUrl: string = 'server/index.php';
    private headerJson: Headers = new Headers({'Content-Type': 'application/json'});
    private job: string = '_s';

    private skills: Skill[] = new Array();
    private responseStatus: any[] = [];

    constructor(private http: Http) {}

    getSkills(): Promise<Skill[]> {
        if (this.skills.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.skills);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';

            return this.http
                .post(this.serverUrl,
                     JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then(response => {
                    this.skills = response.json();
                    return response.json() as Skill[];
                })
                .catch(this.handleError);
        }
    }

    getSkill(id: number): Promise<Skill> {
        if (this.skills.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.skills[id]);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = `${id}`;

            return this.http
                .post(this.serverUrl,
                    JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then((response: Response) => response.json() as Skill)
                .catch( this.handleError );
        }
    }
    create(skill: Skill): Promise<Skill> {

        const addData: Skill = skill;
        addData['job'] = 'add' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(addData),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                this.skills[skill.id] = skill;
            })
            .catch(this.handleError);
    }

    update(skill: Skill): Promise<Skill> {

        const updata: Skill = skill;
        updata['job'] = 'update' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(updata),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();

                for (let key in skill) {
                    if (skill.hasOwnProperty(key)) {
                        this.skills[skill.id][key] = skill[key];
                    }
                }
                this.skills[skill.id] = skill;
            })
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        return this.http
            .delete(this.serverUrl, {headers: this.headerJson})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
