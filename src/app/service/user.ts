export class User {
    id:             number;
    Name:           string;
    Username:       string;
    Password:       string;
    Group:          number;
    Level:          number;
    Create:         number;
    Update:         number;
    Validity:       number;
}
