export class Monster {
    id:             number;
    Name:           string;
    Ruby:           string;
    Attribute:      number;
    SubAttribute:   number;
    Rarity:         number;
    Cost:           number;
    Type:           number;
    SubType:        number;
    ThirdType:      number;
    HP:             number;
    Attack:         number;
    Cure:           number;
    Skill:          number;
    Reader:         number;
    Arousal:        number;
    Evolution:      number;
    etc3:           string;
    etc4:           string;
}
