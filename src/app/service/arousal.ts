export class Arousal {
    id:             number;
    icon:           number;
    Name:           string;
    Type:           number;
    Program:        number;
    Propatie:       string;
    Description:    string;
    etc3:           string;
    etc4:           string;
}
