import { Injectable } from '@angular/core';
import { Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Arousal } from './arousal';
@Injectable()
export class ArousalService {

    private serverUrl: string = 'server/index.php';
    private headerJson: Headers = new Headers({'Content-Type': 'application/json'});
    private job: string = '_a';

    private arousals: Arousal[] = new Array();
    private responseStatus: any[] = [];

    constructor(private http: Http) {}

    getArousals(): Promise<Arousal[]> {
        if (this.arousals.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.arousals);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';

            return this.http
                .post(this.serverUrl,
                     JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then(response => {
                    this.arousals = response.json();
                    return response.json() as Arousal[];
                })
                .catch(this.handleError);
        }

    }

    getArousal(id: number): Promise<Arousal> {
        if (this.arousals.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.arousals[id]);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = `${id}`;

            return this.http
                .post(this.serverUrl,
                    JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then((response: Response) => response.json() as Arousal)
                .catch(this.handleError);
        }
    }
    create(arousal: Arousal): Promise<Arousal> {

        arousal['job'] = 'add' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(arousal),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                this.arousals.push(arousal);
            })
            .catch(this.handleError);
    }

    update(arousal: Arousal): Promise<Arousal> {

        const update: Arousal = arousal;
        update['job'] = 'update' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(update),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.arousals[arousal.id] = arousal;

                this.responseStatus = response.json();
                for (let key in arousal) {
                    if (arousal.hasOwnProperty(key)) {
                        this.arousals[(arousal.id)][key] = arousal[key];
                    }
                }
                this.arousals[(arousal.id)] = arousal;
            })
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        return this.http
            .delete(this.serverUrl, {headers: this.headerJson})
            .toPromise()
            .then(() => {
                this.arousals.splice(id, 1);
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
