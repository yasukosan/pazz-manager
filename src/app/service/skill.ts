export class Skill {
    id:             number;
    Name:           string;
    Start:          number;
    Max:            string;
    MaxLevel:       number;
    Type:           string;
    Attribute:      string;
    Mtype:          string;
    Target:         number;
    Program:        number;
    Propatie:       string;
    Description:    string;
    etc3:           string;
    etc4:           string;
}
