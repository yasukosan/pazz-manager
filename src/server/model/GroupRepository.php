<?php

class GroupRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id","int","0",1),
            "Groupame"		=>array("name","text","",1),
            "Level"			=>array("level","int","0",1),
            "Description"	=>array("Description","text","",1),
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO group(
                id,Groupname,Level,Description
                )
            VALUES(
                :id,:Groupname,:Level,:Description
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Groupname'	=> $status['Groupname'],
                    ':Level'		=> $status['Level'],
                    ':Description'	=> $status['Description'],
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE group SET
            Groupname	= :Groupname,
            Level		= :Level,
            Description	= :Description,
        WHERE
            id			= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Groupname'	=> $status['Groupname'],
                    ':Level'    	=> $status['Level'],
                    ':Description'	=> $status['Description'],
        ));
    }

    public function getAll(){
        $sql = "
        SELECT
            id,
            Groupname,
            Level,
            Description
        FROM
            group
        ";

        return $this->fetchAll($sql,array());
    }


    public function getById($status){
        $sql = "
        SELECT
            id,
            Groupname,
            Level,
            Description
        FROM
            group
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

}

