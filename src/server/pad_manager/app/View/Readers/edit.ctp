<!-- File: /app/View/Posts/edit.ctp -->

<h1>リーダースキル</h1>
<?php
echo $this->Form->create('Reader');
echo $this->Form->input('Id', array('label' => '番号'));
echo $this->Form->input('Name',array('label' => '名前'));
echo $this->Form->input('Type',array('label' => 'スキルタイプ'));
echo $this->Form->input('Attribute',array('label' => '対象属性'));
echo $this->Form->input('MType',array('label' => '対象タイプ'));
echo $this->Form->input('Rate',array('label' => '倍率'));
echo $this->Form->input('Program',array('label' => 'プログラム'));
echo $this->Form->input('Propatie',array('label' => 'プログラム引数'));
echo $this->Form->input('Description',array('label' => '詳細'));


echo $this->Form->end('Save Post');
?>