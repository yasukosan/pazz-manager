
var word = {

    hoge:""

    ,setuped: false
    ,setup: function(){
        if(!this.setuped){
            this.setFilterEvent();
            this.setuped = true;
        }
    }

    ,showMonsterList: function(){

        $("#monster-box2").empty();

        for(var i in monster.monsters){

            if(i > 0){

                var ss = skill.skills[i];

                if(this.dmapFilter[i] == undefined){
                    this.dmapFilter[i] = 0;
                }

                if(this.dmapFilter[i] < 1){
                    $("ul#monster-box2").append("" +
                            "<li data-role='"+m["id"]+"'>" +
                            "<div class='ml"+m["id"]+"' alt='"+m["Name"]+"'></div>" +
                            "<div >"+m["id"]+"</div>" +
                            "<div >"+m["Name"]+"</div>" +
                            "</li>");
                }
            }
        }

        return function(){
            skilltype.setViewEvent();
        }();
    }

    ,monsFilter:[]
    ,searched: true
    ,setFilterEvent: function(){
        if(!word.setuped){
            $("#list-search-word").bind("keyup",function(){
                if(word.searched){
                    word.searched = false;
                    setTimeout(word.buildList, 500);
                }
            });
        }
    }

    ,buildList: function(){
        $("#listbox").empty();
        String.prototype.toOneNumeric = function(){
            return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
            });
        }
        var w = $("#list-search-word").val();
        var wcheck = w.toOneNumeric();

        if(monster.Target == 'skill'){
            var list = monster.Skills;
        }else if(monster.Target == 'leader'){
            var list = monster.Leaders;
        }

        if(isFinite(wcheck) && $("#list-search-word").val() != ""){

            for(var i in list){

                var num = list[i]["id"].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

            return function(){
                word.setViewEvent();
            }();
        }else if($("#word_search").val() != ""){

            for(var i in list){

                var num = list[i].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

            return function(){
                word.addList();
            }();
        }else{
            word.searched = true;
        }
    }

    ,clear:function(){
        $("#list-search-word").val("");
    }
    ,addList: function(){
        $("ul#listbox > li").each(function(){
            $(this).bind("click",function(){
                var num = $(this).data('role');
                monster.addList(num);
            })
        })

        return function(){

            word.searched = true;
        }();
    }

}
