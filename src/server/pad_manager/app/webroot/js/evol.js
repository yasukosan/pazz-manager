
$(document).ready(function(){

    evol.getURL();
    evol.getData();
    $("#listview").show();
    $("#EvolutionTmp").bind("click",function(){
        $("#evol-temp").show();
    });

});

var evol = {

    hoge:""
    ,setuped: false

    ,Monsters:{}
    ,Numbers:{}
    ,URL:""

    ,Target:""

    ,showList: function(val){
        evol.Target = $(val).find("p:eq(1)").attr("id");
        $("#evol-list").show();
        evol.setEvent();
        $("ul#listbox").empty();
    }
    ,hideList: function(target){
        evol.Target = "";
        $("ul#listbox").empty();
        $("#evol-list").hide();
    }

    ,addList:function(num){
        var target = evol.Target.charAt(0).toUpperCase() + evol.Target.slice(1);

        $("#Evolution"+target).val(num);
        $("#"+this.Target).empty();
        $("#"+this.Target).append(this.Monsters[num]);

        this.hideList();
    }

    ,addTemp:function(num){
        $("#EvolutionTmp").val(num);
        $("#evol-temp").hide();
    }

    ,getURL: function(){
        this.URL = $("div#access").html();
    }

    //モンスターデータの取得
    ,getData:function(){
        AjaxLoader.load(
            "job=mons&id=all",
            this.URL+"/testapi",
            "",
            function(target,msg){

                var val = JSON.parse(msg);

                if(val['status']){
                    evol.Monsters = val['result'];

                    for(var i in val['result']){
                        evol.Numbers[i] = i;
                    }
                }else{
                    alert('ERROR'+val['error']['message']);
                }

                evol.convSelected();

        });
    }

    ,searched: true
    ,setEvent: function(){
        if(!evol.setuped){
            $("#list-search-word").bind("keyup",function(){
                if(evol.searched){
                    evol.searched = false;
                    setTimeout(evol.buildList, 500);
                }
            });
        }
    }

    ,selectMons: function(val){

    }

    ,buildList: function(){
        $("#listbox").empty();
        String.prototype.toOneNumeric = function(){
            return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
            });
        }
        var w = $("#list-search-word").val();
        var wcheck = w.toOneNumeric();

        var list = evol.Monsters;

        if(isFinite(wcheck) && $("#list-search-word").val() != ""){

            for(var i in list){

                if(i == w){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

        }else if($("#list-search-word").val() != ""){

            for(var i in list){

                var num = list[i].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

        }else{
            return function(){
                evol.searched = true;
            }();
        }

        evol.addEvent();
    }
    ,clear:function(){
        $("#word_search").val("");
    }
    ,addEvent: function(){
        $("ul#listbox > li").each(function(){
            $(this).bind("click",function(){
                var num = $(this).data('role');
                evol.addList(num);
            })
        })

        return function(){

            evol.searched = true;
        }();
    }


    ,convSelected:function(){
        if($("#EvolutionEb").val() != ""){
            $("#eb").empty();
            $("#eb").append(evol.Monsters[$("#EvolutionEb").val()]);
        }
        for(var i = 1; i <= 5; i++){
            if($("#EvolutionEam"+i).val() != ""){
                $("#eam"+i).empty();
                $("#eam"+i).append(evol.Monsters[$("#EvolutionEam"+i).val()]);
            }
            for(var j = 0; j <= 4; j++){
                if($("#EvolutionEm"+i+"-"+j).val() != ""){
                    $("#em"+i+"-"+j).empty();
                    $("#em"+i+"-"+j).append(evol.Monsters[$("#EvolutionEm"+i+"-"+j).val()]);
                }
            }
        }
    }

}
