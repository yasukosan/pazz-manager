
$(document).ready(function(){

    skill.getURL();
    skill.getSkillData();
    $("#listview").show();

});

var skill = {

    hoge:""

    ,Skills:{}
    ,URL:""

    ,Target:""

    ,setEvent:function(){
        this.setFilterEvent();
    }


    ,getURL: function(){
        this.URL = $("div#access").html();
    }

    //モンスターデータの取得
    ,getSkillData:function(){
        AjaxLoader.load(
            "job=skill&id=all",
            this.URL+"/testapi",
            "",
            function(target,msg){

                var val = JSON.parse(msg);

                if(val['status']){
                    skill.Skills = val['result'];
                }else{
                    alert('ERROR'+val['error']['message']);
                }

                skill.setEvent();

                $("#SkillId").val(skill.Skills.length);

        });
    }

    ,searched: true
    ,setFilterEvent: function(){
        if(!word.setuped){
            $("#SkillName").bind("keyup",function(){
                if(skill.searched){
                    skill.searched = false;
                    setTimeout(skill.buildList, 500);
                }
            });
        }
    }

    ,buildList: function(){
        $("#listbox").empty();
        String.prototype.toOneNumeric = function(){
            return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
            });
        }
        var w = $("#SkillName").val();
        var wcheck = w.toOneNumeric();

        var list = skill.Skills;

        if(isFinite(wcheck) && $("#SkillName").val() != ""){

            for(var i in list){

                var num = list[i]["id"].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }


        }else if($("#word_search").val() != ""){

            for(var i in list){

                var num = list[i].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

        }

        return function(){
            skill.searched = true;
        }();

    }

}
