var edit = {

    hoge:""
    ,editDrop: ""
    ,editTarget:false


    ,editStart		: false
    ,lastTab: false

    ,showEdit: function(){
        $("#pazzle-view").css({
            transform:"scale(1.5,1.5)",
            top: "70px",
            left: "70px",
            zIndex:"1000000"
                });
        this.switchView();
        $("#drop-list").show();
        edit.tmpSetup();
        edit.start();
    }

    ,closeEdit: function(){
        this.switchClose();
        $("#drop-list").hide();
        $("#pazzle-view").css({
            transform:"scale(1,1)",
            top: "0px",
            left: "0px",
            zIndex:"1"});
        edit.start();
    }

    ,switchView: function(){
        $("#edit_switch").hide();
        $("#checksum").hide();
        $("#edit_close").show();
    }
    ,switchClose: function(){
        $("#edit_switch").show();
        $("#checksum").show();
        $("#edit_close").hide();
    }
    ,start: function(){

        if(!edit.editStart){
            pazzle.delAction();
            edit.editStart = true;
            $("div.pazzle").css({backgroundColor:"rgba(100,100,100,0.4)"});

            edit.setEdit();

        }else if(edit.editStart){
            edit.editStart = false;
            $("div.pazzle").css({backgroundColor:"rgba(250,250,250,1)"});
            edit.delAction();
            pazzle.setAction();
        }
    }

    ,setEdit: function(){
        $("ul.drop-type").find("li").each(function(){
            $(this).bind("click",function(){
                if(edit.editDrop != $(this).attr("class")){
                    $(edit.editTarget).css({backgroundColor:"rgba(250,250,250,1)"});
                }

                edit.editDrop = $(this).attr("class");
                edit.editTarget = $(this);
                $(edit.editTarget).css({backgroundColor:"rgba(100,100,100,0.6)"});
            });
        });

        $("div.pazzle > ul").find("li").each(function(){
            $(this).bind("click",function(e){
                $(this).removeClass()
                $(this).addClass(edit.editDrop);
            });
        });
    }

    ,delAction: function(){
        $("ul.drop-type").find("li").each(function(){
            $(this).unbind("mousedown");
        });
        $("div.pazzle > ul").find("li").each(function(){
            $(this).unbind("mousedown");
        });
    }

    ,show:""
    ,tmpName: {1:"1color",2:"2color",3:"3color",5:"5color",6:"6color",a:"randam"}
    ,tmpSetup: function(){
        if(!edit.lastTab){
            edit.lastTab = 1;
        }
        for(var i in edit.tmpName){
            if(edit.lastTab != i){
                $("#"+edit.tmpName[i]).hide();
            }
        }
    }
    ,chengeTmp: function(id){
        if(edit.lastTab != id){
            $("#"+edit.tmpName[edit.lastTab]).hide();
            edit.lastTab = id;
            $("#"+edit.tmpName[id]).show();
        }
    }

    ,chengeMap: function(list){
        var drop = pazzle.dropColors;
           var count = list.length;
           $(".pazzle > ul").find("li").each(function(){

            $(this).removeClass();
            $(this).addClass(drop[list[~~(Math.random()*(count-0)+0)]]);
           });
    }

}




