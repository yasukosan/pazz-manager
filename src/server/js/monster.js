
var monster = {

    hoge:""
    ,monsters:false
    ,show: false
    ,selectCell:0

    ,skillMap: []
    ,skillType: []
    ,leaderMap: []
    ,leaderType: []
    ,nameMap: []

    ,selectedMonster: {}

    ,screen: [0,1,0,0,0,0,0]

    ,switchShow:false

    ,toolsShow:false

    ,settoolsSwitch:function(){
        $("#tools-switch").hover(function(){
                monster.toolsSwitch();
            },function(){});

    }

    ,toolsSwitch: function(){
        if(this.toolsShow){
            $("#search-tools").css({width:"950px",height:"600px"});
            $("div#tools-switch").empty();
            $("div#tools-switch").append("消す");
            this.toolsShow = false;
        }else{
            $("#search-tools").css({width:"10px",height:"10px"});
            $("div#tools-switch").empty();
            $("div#tools-switch").append("表示");
            this.toolsShow = true;
        }
    }
    ,search: function(num){
        this.selectCell = num;
        if(this.show){
            $("#monster-search").hide();
            this.show = false;
        }else{
            $("#monster-search").show();
            this.show = true;

            if(!monster.monsters){
                this.getMonsterData();
            }
        }
        if(!this.switchShow){
            this.switchSearch();
            this.switchShow = true;
        }
    }


    //検索タイプの切り替え
    ,switchSearch: function(){
        $("#search-type-list").find("li").each(function(){
            $(this).bind("click",function(){
                var n = $(this).index() + 1;
                if(!monster.screen[n]){
                    var h = $.inArray(1, monster.screen);
                    if(h > 0){
                        $("#search-type"+h).hide();
                        monster.screen[h] = 0;
                    }
                }

                $("#search-type"+n).show();
                if(n == 5){
                    leadermap.setup();
                }
                if(n == 4){
                    dropmap.setup();
                }
                if(n == 3){
                    skilltype.setup();
                }
                if(n == 2){
                    word.setup();
                }
                monster.screen[n] = 1;
            });
        });
    }

    //モンスターデータの取得
    ,getMonsterData:function(){
        AjaxLoader.load(
                "job=getm&id=all",
                'aj',
                "",
                function(target,msg){
                    monster.monsters = eval(msg);
                    for(var i = 0; i <= (monster.monsters.length - 1); i++){
                        var m = monster.monsters[i];
                        $("ul#monster-box").append("" +
                                "<li data-role='"+m["id"]+"'>" +
                                //"<div class='ml"+m["id"]+"' alt='"+m["Name"]+"'></div>" +
                                "<div class='mimage-l'><img src='img/m/"+m["id"]+".jpg' /></div>" +
                                "<div >"+m["id"]+"</div>" +
                                "<div >"+m["Name"]+"</div>" +
                                "</li>");

                        monster.showHideList.push([m["id"],0]);
                        monster.skillMap.push(m["Skill"]);
                        monster.leaderMap.push(m["Reader"]);
                        monster.leaderType.push(leader.leaderSkills[m["Reader"]]["Program"]);
                        monster.nameMap.push(m["Name"]);

                    }
                    return function(){
                        monster.settoolsSwitch();
                        monster.viewPropatie();
                        monster.setRefine();
                    }();
                }
            );
    }
    /**
     *
     * ステータス表示処理
     *
     */
    ,selected:{}
    ,propertyList:["無し","red","blue","green","yellow","purple","heart"]
    ,typeList:["無し","ドラゴン","体力","攻撃","回復","バランス","神","悪魔","進化","覚醒","強化合成","特別保護"]
    ,viewPropatie: function(){
        $("ul#monster-box").find("li").each(function(){
            $(this).bind("click",function(){
                monster.buildView(this);
            });
        });
    }

    ,buildView:function(target){

        if($(target).data("role") == "" || $(target).data("role") == undefined){
            return false;
        }
        var id = $(target).data("role");
        if($(target) != $(monster.selected)){
            $(monster.selected).css({boxShadow:"none"});
            $(target).css({boxShadow:"0px 0px 10px rgba(1,1,1,1)"});
            monster.selected = $(target);
        }
        monster.propatieReset();

        var Skill = skill.skills[monster.monsters[(id - 1)]["Skill"]];
        var Leader = leader.leaderSkills[monster.monsters[(id - 1)]["Reader"]];
        var mdata = monster.monsters[(id - 1)];

        this.selectedMonster = mdata;

        if(monster.propertyList[mdata["Attribute"]] == "無し"){
            var attri = "無し";
        }else{
            var attri = "<p class='"+monster.propertyList[mdata["Attribute"]]+" drop'></p>";
        }
        if(monster.propertyList[mdata["SubAttribute"]] == "無し"){
            var sattri = "無し";
        }else{
            var sattri = "<p class='"+monster.propertyList[mdata["SubAttribute"]]+" drop'></p>";
        }

        var ar = monster.arousalList(monster.monsters[(id - 1)]["Arousal"]);

        //$("#ms-img").addClass("ml"+(id));
        $("#ms-img").append('<img class="mimage_l" src="img/m/'+mdata["id"]+'.jpg" />');
        $("#ms-number").append(mdata["id"]);
        $("#ms-name").append(mdata["Name"]);
        $("#ms-attribute").append(attri);
            $("#ms-subattribute").append(sattri);
            $("#ms-type").append(monster.typeList[mdata["Type"]]);
            $("#ms-subtype").append(monster.typeList[mdata["SubType"]]);
            $("#ms-thirdtype").append(monster.typeList[mdata["ThirdType"]]);
            $("#ms-cost").append(mdata["Cost"]);
            $("#ms-rarity").append(mdata["Rarity"]);
            $("#ms-hp").append(mdata["HP"]);
            $("#ms-attack").append(mdata["Attack"]);
            $("#ms-cure").append(mdata["Cure"]);
            $("#ms-skill-na").append(Skill["Name"]);
            $("#ms-skill-l").append("Max:"+Skill["Max"]);
        $("#ms-skill-pro").append(Skill["Description"]);
        $("#ms-leader-na").append(Leader["Name"]);
        $("#ms-leader-pro").append(Leader["Description"]);
        $("#ms-arousal-pro").append(ar);



        //進化ボタン表示
        evolution.showSwitch();
    }


    ,propatieReset: function(){
        $("#ms-img").empty();
        $("#ms-number").empty();
        $("#ms-name").empty();
        $("#ms-attribute").empty();
        $("#ms-subattribute").empty();
        $("#ms-type").empty();
        $("#ms-subtype").empty();
        $("#ms-cost").empty();
        $("#ms-rarity").empty();
        $("#ms-hp").empty();
        $("#ms-attack").empty();
        $("#ms-cure").empty();
        $("#ms-skill-na").empty();
        $("#ms-skill-l").empty();
        $("#ms-skill-pro").empty();
        $("#ms-leader-na").empty();
        $("#ms-leader-pro").empty();
        $("#ms-arousal-na").empty();
        $("#ms-arousal-pro").empty();
    }

    ,arousalList: function(arousal){

        if(arousal != undefined && arousal.length >= 1){
            var ret = "";
            var ar = arousal.split(",");

            for(var i = 0; i <= ar.length - 1; i++){
                ret += "<p class='a"+ar[i]+"'></p>";
            }

            return ret;

        }else{
            return "無し"
        }

    }


    /**
     *
     * 絞込
     *
     */

    ,showHideList:[]
    ,setRefine: function(){
        this.setSearchEvent("attribute","Attribute","SubAttribute");
        this.setSearchEvent("type","Type","SubType");
        this.setSearchEvent("arousal","Arousal",false);
        this.setSearchEvent("cost","Cost",false);
        return function(){
            monster.setSearchEvent("rea","Rarity",false);
        }();

    }

    ,costMap: [11,15,21,19]
    ,reaMap:  [5,7,1,4]

    ,searchSwitch:{
        "attribute":[0,0,0,0,0,0],
        "type":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        "arousal":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        "cost":[0,0,0,0,0],
        "rea":[0,0,0,0,0]
    }

    ,setSearchEvent: function(search,colom1,colom2){
        $("#search-"+search).find("li").each(function(){
            $(this).bind("click",function(){
                $("ul#monster-box").empty();

                var num = $(this).index() + 1;
                var id = $(this).index();

                if(monster.searchSwitch[search][num] == 0){
                    var on = true;
                    monster.searchSwitch[search][num] = 1;
                    $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                }else{
                    var on = false;
                    monster.searchSwitch[search][num] = 0;
                    $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                }

                for(var i = 0; i <= monster.monsters.length - 1; i++){
                    //覚醒絞り込み処理
                    if(search == "arousal"){
                        var arousal = monster.monsters[i][colom1].split(',');
                        if(arousal.indexOf(String(num)) < 0
                            || monster.monsters[i][colom1].length < 2){
                            if(on){
                                monster.showHideList[i][1]++;
                            }else{
                                monster.showHideList[i][1]--;
                            }
                        }
                    //コスト絞り込み処理
                    }else if(search == "cost"){
                        var cost = Number(monster.monsters[i][colom1]);
                        if(cost >= monster.costMap[id]
                            && id != 3){
                            if(on){
                                monster.showHideList[i][1]++;
                            }else{
                                monster.showHideList[i][1]--;
                            }
                        }else if(cost <= monster.costMap[id] && id == 3){
                            if(on){
                                monster.showHideList[i][1]++;
                            }else{
                                monster.showHideList[i][1]--;
                            }
                        }
                    //レア度絞り込み処理
                    }else if(search == "rea"){
                        var rea = Number(monster.monsters[i][colom1]);
                        if(id < 2){
                            if(rea >= monster.reaMap[id]){
                                if(on){
                                    monster.showHideList[i][1]++;
                                }else{
                                    monster.showHideList[i][1]--;
                                }
                            }
                        }else {
                            if(rea <= monster.reaMap[id]){
                                if(on){
                                    monster.showHideList[i][1]++;
                                }else{
                                    monster.showHideList[i][1]--;
                                }
                            }
                        }
                    }else if(monster.monsters[i][colom1] != num){
                        if(on){
                            if(colom2 != false && monster.monsters[i][colom2] != num){
                                   monster.showHideList[i][1]++;
                            }
                        }else{
                            if(colom2 != false && monster.monsters[i][colom2] != num){
                                monster.showHideList[i][1]--;
                            }
                        }
                    }

                    if(monster.showHideList[i][1] == 0){
                        $("ul#monster-box").append("" +
                                "<li data-role='"+monster.monsters[i]["id"]+"'>" +
                                //"<div class='ml"+monster.monsters[i]["id"]+"' alt='"+monster.monsters[i]["Name"]+"'></div>" +
                                "<div class='mimage-l'><img src='img/m/"+monster.monsters[i]["id"]+".jpg' /></div>" +
                                "<div >"+monster.monsters[i]["id"]+"</div>" +
                                "<div >"+monster.monsters[i]["Name"]+"</div>" +
                                "</li>");
                    }
                }
                return function(){
                    monster.viewPropatie();
                }();
            })
        });
    }

    ,select:function(){
        var id = $(monster.selected).data("role");
        partie.PartieList[monster.selectCell] = monster.monsters[(id - 1)];
        data.PartieList[0][monster.selectCell] = monster.monsters[(id - 1)];

        return function(){
            partie.setMonster(monster.selectCell);
            monster.search();
        }();
    }


    ,sameView: false
    ,sameSkill: function(){
        if(!this.sameView){
            $("#monster-skill-same").show();
            this.sameView = true;
        }
        var sk = this.selectedMonster["Skill"];
        $("#monster-skill-same div p").empty();
        $("#monster-skill-same ul").empty();

        var c = 0;
        if(sk > 0){
            $("#monster-skill-same div p").append("<p>"+skill.skills[sk]["Name"]+"</p>");
            for(var i in monster.monsters){
                var mm = monster.monsters[i];
                if(mm["Skill"] == sk){
                    $("#monster-skill-same ul").append("" +
                        "<li data-role="+mm["id"]+"><p class='mimage-s'><img src='img/m/"+mm["id"]+".jpg'></p></li>");
                    c++;
                }
            }
        }else{
            $("#monster-skill-same div p").append("スキル無し")
        }
        return function(){
            $("#monster-skill-same ul").find("li").each(function(){
                $(this).bind("click",function(){
                    monster.buildView(this);
                });
            });
        }();
    }
    ,sameClose: function(){
        this.sameView = false;
        $("#monster-skill-same").hide();
    }


}
