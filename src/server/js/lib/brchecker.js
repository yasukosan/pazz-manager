var brchecker = {

    agent: window.navigator.userAgent.toLowerCase()
    ,app: window.navigator.appVersion.toLowerCase()
    ,ua: 'unknown'
    ,ios: ''
    ,ios_ver: ''


    ,check: function(){

        if (this.agent.indexOf('iphone') != -1) {
            this.ua = 'iphone';
            var ios = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
              ios_ver = [parseInt(ios[1], 10), parseInt(ios[2], 10), parseInt(ios[3] || 0, 10)];

              var meta = document.createElement('meta');
              meta.setAttribute('name', 'viewport');
              meta.setAttribute('content', 'width=initial-scale=1.0, minimum-scale=0.45, maximum-scale=0.45, width=device-width, user-scalable=0');
              document.getElementsByTagName('head')[0].appendChild(meta);

        } else if (this.agent.indexOf('ipad') != -1) {
            this.ua = 'ipad';
              var meta = document.createElement('meta');
              meta.setAttribute('name', 'viewport');
              meta.setAttribute('content', 'width=initial-scale=1.0, minimum-scale=0.8, maximum-scale=0.8, width=device-width, user-scalable=0');
              document.getElementsByTagName('head')[0].appendChild(meta);
        } else if (this.agent.indexOf('ipod') != -1) {
            this.ua = 'ipod';
            var meta = document.createElement('meta');
              meta.setAttribute('name', 'viewport');
              meta.setAttribute('content', 'width=initial-scale=1.0, minimum-scale=0.8, maximum-scale=0.8, width=device-width, user-scalable=0');
              document.getElementsByTagName('head')[0].appendChild(meta);
        } else if (this.agent.indexOf('android') != -1) {
            this.ua = 'android';
            var meta = document.createElement('meta');
              meta.setAttribute('name', 'viewport');
              meta.setAttribute('content', 'width=initial-scale=1.0, minimum-scale=0.8, maximum-scale=0.8, width=device-width, user-scalable=0');
              document.getElementsByTagName('head')[0].appendChild(meta);
        }else if (this.agent.indexOf('msie') != -1) {
            this.ua = 'ie';
          if (this.app.indexOf('msie 6.') != -1) {
              this.ua = 'ie6';
          } else if (this.app.indexOf('msie 7.') != -1) {
              this.ua = 'ie7';
          } else if (this.app.indexOf('msie 8.') != -1) {
              this.ua = 'ie8';
          } else if (this.app.indexOf('msie 9.') != -1) {
              this.ua = 'ie9';
          } else if (this.app.indexOf('msie 10.') != -1) {
              this.ua = 'ie10';
          }
        } else if (this.agent.indexOf('chrome') != -1) {
            this.ua = 'chrome';

        } else if (this.agent.indexOf('safari') != -1) {
            this.ua = 'safari';
        } else if (this.agent.indexOf('gecko') != -1) {
            this.ua = 'firefox';
        } else if (this.agent.indexOf('opera') != -1) {
            this.ua = 'opera';

        } else if (this.agent.indexOf('mobile') != -1) {
            this.ua = 'mobile';
        };
    }


}
