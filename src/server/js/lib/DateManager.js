var DateManager = {

    hoge:""

    ,DateObj:false
    ,Year:0
    ,Month:0
    ,Date:0
    ,Week:0
    ,set:false

    ,get: function(){
        this.checkSetup();
        return [this.Year,this.Month,this.Date,this.Week];
    }

    ,checkSetup: function(){
        var check = Object.prototype.toString;

        if(!this.set && check.call(this.set) != "object"){
            this.set = true;
            this.setFullDate();
        }
    }

    ,setFullDate: function(){

        this.checkSetup();

        if(!this.DateObj){
            this.DateObj = new Date();
        }
        this.Year = this.DateObj.getFullYear();
        this.Month = this.DateObj.getMonth() + 1;
        this.Date = this.DateObj.getDate();
        this.Week = this.DateObj.getDay();
    }

    ,slideDate: function(num){
        this.checkSetup();
        var Add = num * 864000000;
        var Target = this.DateObj.getTime() + Add;
        this.DateObj.setTime(Target);

        this.dateValidate();
    }
    ,slideMonth: function(num){
        this.checkSetup();
        var Add = this.Month - num;
        if(Add <= 0){
            this.DateObj.setYear(this.Year - 1);
            this.DateObj.setMonth(11 + Add);
        }else if(Add >= 13){
            this.DateObj.setYear(this.Year + 1);
            this.DateObj.setMonth(Add - 11);
        }

        this.dateValidate();
    }

    ,setDate: function(num){
        this.checkSetup();
        if(num <= 31 && num >= 1){
            this.DateObj.setDate(num)
            this.dateValidate();
        }else{
            return false;
        }

    }

    ,setMonth: function(num){
        this.checkSetup();
        if(num <= 12 && num >= 1){
            this.DateObj.setMonth(12 + Add - 1);
            this.dateValidate();
        }else{
            return false;
        }

    }


    ,dateValidate: function(){
        dt=new Date(this.Year,this.Month - 1,this.Date);
        if(dt.getFullYear()==this.Year
                && dt.getMonth()==this.Month-1
                && dt.getDate()==this.Date){

            return true;
        }else{
            this.setLastDate();
            return false;
        }
    }

    ,setLastDate: function(){
        this.DateObj.setMonth(this.DateObj.getMonth() + 1);
        this.DateObj.setDate(0);

        this.setFullDate();
    }
}
