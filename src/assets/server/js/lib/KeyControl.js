$(function(){
  //------------------------
  // jQueryキー制御サンプル
  // return値falseによりキーキャンセル
  //------------------------
  $(document).keydown(function(event){
    // クリックされたキーのコード
    var keyCode = event.keyCode;
    // Ctrlキーがクリックされたか (true or false)
    var ctrlClick = event.ctrlKey;
    // Altキーがクリックされたか (true or false)
    var altClick = event.altKey;
    // キーイベントが発生した対象のオブジェクト
    var obj = event.target;

    // ファンクションキーを制御する
    // 制限を掛けたくない場合は対象から外す
    if(keyCode == 112)return false; // F1キーの制御
    if(keyCode == 113)return false;// F2キーの制御
    if(keyCode == 114)return false; // F3キーの制御
    if(keyCode == 115)return false; // F4キーの制御
    if(keyCode == 116)location.reload(); // F5キーの制御
    if(keyCode == 117)return false; // F6キーの制御
    if(keyCode == 118)return false; // F7キーの制御
    if(keyCode == 119)return false; // F8キーの制御
    if(keyCode == 120)return false; // F9キーの制御
    if(keyCode == 121)return false; // F10キーの制御
    if(keyCode == 122)return false; // F11キーの制御
    if(keyCode == 123)return false; // F12キーの制御

    // バックスペースキーを制御する
    if(keyCode == 8){
      // テキストボックス／テキストエリアを制御する
      if(obj.tagName == "INPUT"
          && obj.type == "TEXT"
          || obj.type == "text"
          || obj.tagName == "TEXTAREA"
          || obj.tagName == "textarea"){

        // 入力できる場合は制御しない
        if(!obj.readOnly && !obj.disabled){
          return true;
        }
      }
      return false;
    }

    // Alt + ←を制御する
    if(altClick && (keyCode == 37 || keyCode == 39)){
      return false;
    }

    // Ctrl + Nを制御する
    if(ctrlClick && keyCode == 78){
      return false;
    }
  });
});