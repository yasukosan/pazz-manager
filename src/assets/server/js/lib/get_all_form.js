
var getAllForm = {

    targetForm: {
        "addCategory"	: "#CategoryForm",
        "editCategory"	: "#CategoryForm",
        "addItem"		: "#CategoryForm",
        "editItem"		: "#CategoryForm"
        }

    ,targetContent: {
        "addCategory"	: "#AddCategory",
        "editCategory"	: "#EditCategory",
        "addItem"		: "#AddItem",
        "editItem"		: "#EditItem"
        }

    //取得するタグ一覧
    ,targetTag: ["input","select","textarea","checkbox","radio"]
    ,target   : {}
    ,data     : {}
    ,post     : ""
    ,num      : 0
    ,count    : 0
    ,countTag : 0
    ,url      : ""

    ,buildJob: false

    ,search: function(obj,target,buildJob){

        this.reset();

        if(target !== null){
            this.target = target;
            if(!buildJob){
                this.url=$(getAllForm.targetForm[target]).attr("action");
            }else{
                this.buildJob = buildJob;
            }

            this.getTarget();
        }
    }

    ,getTarget: function(){

        this.countTag = 0;

        var targetArray = this.tagChecker();


        for(var value in targetArray){

            this.num = $(this.targetContent[this.target]).find(this.targetTag[value]).length;

            $(this.targetContent[this.target]).find(this.targetTag[value]).each(function(i){

                if($(this).attr("name") !== undefined){

                    getAllForm.data[$(this).attr("name")] = $(this).val();
                    //$(this).val("");
                }

                if(i === (getAllForm.num - 1)){

                    if(getAllForm.count == getAllForm.countTag){

                        if(!getAllForm.buildJob){
                            getAllForm.build();
                        }else{
                            getAllForm.buildJob();
                        }


                    }
                    getAllForm.count++;
                }
            });
        }
    }

    ,build: function(){

        this.post = "";
        var count = 0;

        for(var value in this.data){
            if(count === 0){
                this.post = this.post + value + "=" + this.data[value];
            }else{
                this.post = this.post + "&" + value + "=" + this.data[value];
            }
            count++;
        }
        AjaxLoader.load(
            getAllForm.post,
            getAllForm.url,
            "",
            function(target,msg){
                if(msg == "success"){
                    if(Propaties.Mode === "customer"){
                    }else if(Propaties.Mode === "company"){
                    }
                    AjaxLoader.showstatus("更新完了");
                }else{
                    AjaxLoader.showstatus(msg);
                }
            }
        );
    }

    /*
     *
     */
    ,tagChecker: function(){
        var value = {};
        var num =  this.targetTag.length - 1;


        for(var tag in this.targetTag){

            if($(this.targetContent[this.target]).find(this.targetTag[tag]).length > 0
                && isFinite($(this.targetContent[this.target]).find(this.targetTag[tag]).length)){

                value[tag] = this.targetTag[tag];
                this.countTag++;
            }

            if(tag == num){
                this.countTag--;
                return value;
            }
        }
    }

    ,reset: function(){
        this.target   = {};
        this.data     = {};
        this.post     = "";
        this.num      = 0;
        this.count    = 0;
        this.countTag = 0;
        this.url      = "";
    }
}
