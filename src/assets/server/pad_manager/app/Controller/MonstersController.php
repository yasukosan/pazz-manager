<?php

class MonstersController extends AppController {

    var $uses = array('Monster', 'Type','Attribute','Arousal','Skill','Reader','Evolution');

    public $helpers = array('Html','Form');

    public function index($page = null) {

        $page = (isset($page))?$page:1;

        $pages = count(($this->Monster->find('all',
            array(
                'fields' => array('Id')
            )
        )));

        $limit = 100;
        $vb = new ViewBuilder();
        $monsters["Count"] = $vb->pageBuild($pages,$limit);

        $monsters["monster"] = $this->Monster->find('all',
                array(
                        'limit' => $limit,
                        'page' => $page
                ));
        $this->set('monsters',$monsters);
    }

    public function view($id = null){
        if(!$id){
            throw new NotFoundException(_('Invalid post'));
        }

        $post = $this->Monster->findById($id);

        if (!$post){
            throw new NotFoundException(_('Invalid post'));
        }

        $this->set('monster',$post);
    }


    public function add(){

        $prop['type'] = $this->Type->find('list',
                array('fields' => array('id','Name')));
        $prop['arousal'] = $this->Arousal->find('list',
                array('fields' => array('Id','Name')));
        $prop['attribute'] = $this->Attribute->find('list',
                array('fields' => array('Id','Name')));
        $prop['skill'] = $this->Skill->find('list',
                array('fields' => array('Id','Name','Description')));
        $prop['leader'] = $this->Reader->find('list',
                array('fields' => array('Id','Name','Description')));
        $prop['evolution'] = $this->Evolution->find('list',
                array('fields' => array('Id')));

        if($this->request->is('post')){

            $arousal = "";
            for($i = 1; $i <= 9; $i++){
                if($this->request->data["Monster"]["Arousal".$i] != 0){
                    $arousal .= $this->request->data["Monster"]["Arousal".$i].',';
                }
                $this->request->data["Monster"]["Arousal".$i] = null;
            }
            $arousal = substr($arousal,0,-1);
            $this->request->data["Monster"]["Arousal"] = $arousal;

            $this->Monster->create();

            if($this->Monster->save($this->request->data)){
                $this->Session->setFlash(_('Yout post has been saved'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Session->setFlash(_('Unable to add your post.'));
        }
        for($i = 0; $i <= 8; $i++){
               $prop['arousal_val'][$i] = 0;
        }
        $this->set('prop',$prop);
    }

    public function edit($id = null) {

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $prop['type'] = $this->Type->find('list',
                array('fields' => array('id','Name')));
        $prop['arousal'] = $this->Arousal->find('list',
                array('fields' => array('id','Name')));
        $prop['attribute'] = $this->Attribute->find('list',
                array('fields' => array('id','Name')));
        $prop['skill'] = $this->Skill->find('list',
                array('fields' => array('id','Name','Description')));
        $prop['leader'] = $this->Reader->find('list',
                array('fields' => array('id','Name','Description')));
        $prop['evolution'] = $this->Evolution->find('list',
                array('fields' => array('id')));

        $post = $this->Monster->findById($id);

        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $arousal = "";
            for($i = 1; $i <= 9; $i++){
                if($this->request->data["Monster"]["Arousal".$i] != 0){
                    $arousal .= $this->request->data["Monster"]["Arousal".$i].',';
                }
                $this->request->data["Monster"]["Arousal".$i] = null;
            }
            $arousal = substr($arousal,0,-1);
            $this->request->data["Monster"]["Arousal"] = $arousal;
            $this->Monster->id = $id;

            if ($this->Monster->save($this->request->data)) {
                $this->Session->setFlash(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }

        $ar_check=explode(',',$post['Monster']['Arousal']);

        for($i = 0; $i <= 8; $i++){
            if(!isset($ar_check[$i])){
                $prop['arousal_val'][$i] = 0;
            }else{
                $prop['arousal_val'][$i] = $ar_check[$i];
            }
        }
        $this->set('prop',$prop);
    }
    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Session->setFlash(
                    __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Session->setFlash(
                    __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }
}
