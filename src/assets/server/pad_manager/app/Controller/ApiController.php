<?php

class ApiController extends AppController {

    // テスト用モデル
    public $uses = array('Monster','Skill','Reader');

    public function testApi() {

        $this->autoRender = false;

        if(!$this->request->is('ajax')) {
            throw new BadRequestException();
        }

        if($this->data['job'] == 'mons'){

            $result = $this->Monster->find('list',
                array('fields' => array('Id','Name')));

        }else if($this->data['job'] == 'skill'){

            $result = $this->Skill->find('list',
                array('fields' => array('Id','Name')));

        }else if($this->data['job'] == 'lead'){

            $result = $this->Reader->find('list',
                array('fields' => array('Id','Name')));
        }


        $status = !empty($result);
        if(!$status) {
            $error = array(
                    'message' => 'データがありません',
                    'code' => 404
            );
        }

        return json_encode(compact('status', 'result', 'error'));
    }

}
