<!-- File: /app/View/Posts/add.ctp -->

<?php

echo $this->Html->charset();

echo $this->Html->script( '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',array('inline'=>false));
echo $this->Html->script( 'ajaxloader.js');
echo $this->Html->script( 'jquery.tmpl.js');
echo $this->Html->script( 'word.js');
echo $this->Html->script( 'skill.js');

echo $this->Html->css( 'listview.css');

?>


<h1>モンスター情報編集</h1>
<?php
echo $this->Form->create('Skill');
echo $this->Form->input('Id', array('label' => '番号'));
echo $this->Form->input('Name',array('label' => '名前'));
echo $this->Form->input('Start',array('label' => '初期スキルターン'));
echo $this->Form->input('Max',array('label' => 'スキルMAX時スキルターン'));
echo $this->Form->input('MaxLevel',array('label' => '上昇レベル'));
echo $this->Form->input('Type',array('label' => 'スキルタイプ'));
echo $this->Form->input('Attribute',array('label' => 'ターゲット属性'));
echo $this->Form->input('MType',array('label' => 'ターゲットタイプ'));
echo $this->Form->input('Description',array('label' => '詳細'));
echo $this->Form->end('Save Post');
?>
<div id="listtab">
    <ul id="listbox"></ul>
</div>

<div id="access"><?php echo $this->Html->url('/api',true); ?></div>
