<!-- File: /app/View/Posts/add.ctp -->

<?php

echo $this->Html->charset();

echo $this->Html->script( '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',array('inline'=>false));
echo $this->Html->script( 'ajaxloader.js');
echo $this->Html->script( 'jquery.tmpl.js');
echo $this->Html->script( 'word.js');
echo $this->Html->script( 'leader.js');

echo $this->Html->css( 'listview.css');

?>

<h1>モンスター情報編集</h1>
<?php
echo $this->Form->create('Reader');
echo $this->Form->input('Id', array('label' => '番号'));
echo $this->Form->input('Name',array('label' => '名前'));
echo $this->Form->input('Type',array('label' => 'スキルタイプ'));
echo $this->Form->input('Attribute',array('label' => '対象属性'));
echo $this->Form->input('MType',array('label' => '対象タイプ'));
echo $this->Form->input('Rate',array('label' => '倍率'));
echo $this->Form->input('Program',array('label' => 'プログラム'));
echo $this->Form->input('Propatie',array('label' => 'プログラム引数'));
echo $this->Form->input('Description',array('label' => '詳細'));
echo $this->Form->end('Save Post');
?>

<div id="listtab">
    <ul id="listbox"></ul>
</div>

<div id="access"><?php echo $this->Html->url('/api',true); ?></div>