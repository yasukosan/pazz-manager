<html>
<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>

<body>
<h1>リーダースキル</h1>

<table>
    <tr>
        <td>
<p><?php echo $this->Html->link("モンスター", array('action' => '../monsters')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("スキル", array('action' => '../skills')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("リーダースキル", array('action' => '../readers')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("進化表", array('action' => '../evolutions')); ?></p>
        </td>
    </tr>
</table>
<table>
    <tr>
<?php $i = 0;
foreach ($skills["Count"] as $page): ?>
        <?php if(($i % 10) == 0): ?>
    </tr>
    <tr>
        <?php endif; ?>
        <td>
        <?php echo $this->Html->link($page['desc'], array('action' => 'index', $page['page'])); ?>
        </td>
        <?php $i++; ?>
<?php endforeach; ?>
    </tr>
</table>

<p><?php echo $this->Html->link("新規追加", array('action' => 'add')); ?></p>

<table>
    <tr>
        <th>Id</th>
        <th>名前</th>
        <th>編集</th>
        <th>レベル</th>
        <th>タイプ</th>
        <th>属性</th>
        <th>タイプ</th>
        <th>効果</th>
    </tr>

<!-- $post配列をループして、投稿記事の情報を表示 -->

<?php foreach ($skills["Reader"] as $skill): ?>
    <tr>
        <td><?php echo $skill['Reader']['Id']; ?></td>
        <td>
            <?php echo $this->Html->link($skill['Reader']['Name'], array('action' => 'view', $skill['Reader']['Id'])); ?>
        </td>
        <td>
            <?php echo $this->Form->postLink(
                '削除',
                array('action' => 'delete', $skill['Reader']['Id']),
                array('confirm' => 'Are you sure?'));
            ?>
            <?php echo $this->Html->link('編集', array('action' => 'edit', $skill['Reader']['Id'])); ?>
        </td>
        <td>初期：
            <?php echo $skill['Reader']['Type']; ?>
            最大：
            <?php echo $skill['Reader']['Attribute']; ?>
            最大レベル：
            <?php echo $skill['Reader']['MType']; ?>
        </td>
        <td>
            <?php echo $skill['Reader']['Rate']; ?>
        </td>
        <td>
            <?php echo $skill['Reader']['Program']; ?>
        </td>
        <td>
            <?php echo $skill['Reader']['Propatie']; ?>
        </td>
        <td>
            <?php echo $skill['Reader']['Description']; ?>
        </td>
    </tr>
<?php endforeach; ?>

</table>

</body>
</html>