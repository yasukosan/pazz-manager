<?php

class UserRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id",       "int","0",1),   // 固有番号
            "Name"			=>array("name",     "text","",1),   // 名前
            "Username"		=>array("username", "text","",1),   //ログイン名
            "Password"  	=>array("password", "text","",1),   //パスワード
            "Group"			=>array("group",    "int","0",1),   //グループ
            "Level"			=>array("level",    "int","0",1),   //閲覧レベル
            "Create"		=>array("create",   "int","0",1),   //作成日
            "Update"		=>array("update",   "int","0",1),   //更新日
            "Validity"		=>array("validity", "int","0",1),   //有効期限
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO user(
                    `id`, `Name`, `Username`, `Password`, `Group`,
                    `Level`, `Create`, `Update`, `Validity`
                )
            VALUES(
                :id,:Name,:Username,:Password,:Group,:Level,:Create,:Update,:Validity
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Username'		=> $status['Username'],
                    ':Password'	    => $status['Password'],
                    ':Group'	    => $status['Group'],
                    ':Level'		=> $status['Level'],
                    ':Create'		=> $status['Create'],
                    ':Update'		=> $status['Update'],
                    ':Validity'		=> $status['Validity'],
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE user SET
            Name		= :Name,
            Username	= :Username,
            Password	= :Password,
            Group		= :Group,
            Level		= :Level,
            Create		= :Create,
            Update  	= :Update,
            Validity	= :Validity,
        WHERE
            id			= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Username'		=> $status['Username'],
                    ':Password'		=> $status['Password'],
                    ':Group'		=> $status['Group'],
                    ':Level'		=> $status['Level'],
                    ':Create'		=> $status['Create'],
                    ':Update'		=> $status['Update'],
                    ':Validity'		=> $status['Validity']
        ));
    }

    public function getAll(){
        $sql = "
        SELECT
            `id`, `Name`, `Username`, `Password`, `Group`,
            `Level`, `Create`, `Update`, `Validity`
        FROM
            `user`

        ";

        return $this->fetchAll($sql,array());
    }


    public function getById($status){
        $sql = "
        SELECT
            `id`, `Name`, `Username`, `Password`, `Group`,
            `Level`, `Create`, `Update`, `Validity`
        FROM
            `user`
        WHERE
            id		= :id
        ";
        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

    public function getByUsername($status){
        $sql = "
        SELECT
            `id`, `Name`, `Username`, `Password`, `Group`,
            `Level`, `Create`, `Update`, `Validity`
        FROM
            `user`
        WHERE
            Username = :Username
        ";
        return $this->fetch($sql,array(
                ':Username'			=> $status['Username'],
        ));
    }

    public function getByUserIdJoinGroupId($status){
        $sql = "
        SELECT
            user.id,
            user.Name,
            user.Username,
            user.Password,
            user.Group,
            user.Level,
            user.Create,
            user.Update,
            user.Validity,
            group.Groupname
        FROM
            user
        INNER JOIN
            group
        ON
            user.Group = group.id
        WHERE
            user.id		= :id
        ";

        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

    public function getToken(){
        $TOKEN_LENGTH = 16;
        $bytes = openssl_random_pseudo_bytes($TOKEN_LENGTH);
        return bin2hex($bytes);
    }
}

